#include "CommandProcess.h"
#include <thread>
#include "MainFrame.h"

wxBEGIN_EVENT_TABLE(CommandProcess, wxEvtHandler)
#if PANEL_EN_REVERB
    M_EVT_REVERB(wxID_ANY, CommandProcess::OnReverbParamChange)
    M_EVT_REVERB_ENABLE(wxID_ANY, CommandProcess::OnReverbParamChange)
    M_EVT_REVERB_DAMPING(wxID_ANY, CommandProcess::OnReverbParamChange)
    M_EVT_REVERB_DRY(wxID_ANY, CommandProcess::OnReverbParamChange)
    M_EVT_REVERB_FEEDBACK(wxID_ANY, CommandProcess::OnReverbParamChange)
    M_EVT_REVERB_TONE(wxID_ANY, CommandProcess::OnReverbParamChange)
    M_EVT_REVERB_WET(wxID_ANY, CommandProcess::OnReverbParamChange)
#endif
#if PANEL_EN_DELAY
    M_EVT_DELAY(wxID_ANY, CommandProcess::OnDelayParamChange)
    M_EVT_DELAY_FEEDBACK(wxID_ANY, CommandProcess::OnDelayParamChange)
    M_EVT_DELAY_WET(wxID_ANY, CommandProcess::OnDelayParamChange)
    M_EVT_DELAY_TIME(wxID_ANY, CommandProcess::OnDelayParamChange)
    M_EVT_DELAY_ENABLE(wxID_ANY, CommandProcess::OnDelayParamChange)
    M_EVT_DELAY_TONE(wxID_ANY, CommandProcess::OnDelayParamChange)
#endif
#if PANEL_EN_WAH
    M_EVT_WAH(wxID_ANY, CommandProcess::OnWahParamChange)
    M_EVT_WAH_ENABLE(wxID_ANY, CommandProcess::OnWahParamChange)
    M_EVT_WAH_SPEED(wxID_ANY, CommandProcess::OnWahParamChange)
    M_EVT_WAH_FILTER_FREQ_WIDTH(wxID_ANY, CommandProcess::OnWahParamChange)
    M_EVT_WAH_FILTER_FREQ_CENTER(wxID_ANY, CommandProcess::OnWahParamChange)
    M_EVT_WAH_FILTER_Q(wxID_ANY, CommandProcess::OnWahParamChange)
    M_EVT_WAH_FILTER_GAIN(wxID_ANY, CommandProcess::OnWahParamChange)
#endif
#if PANEL_EN_CHORUS
    M_EVT_CHORUS(wxID_ANY, CommandProcess::OnChorusParamChange)
    M_EVT_CHORUS_ENABLE(wxID_ANY, CommandProcess::OnChorusParamChange)
    M_EVT_CHORUS_DRY(wxID_ANY, CommandProcess::OnChorusParamChange)
    M_EVT_CHORUS_WET(wxID_ANY, CommandProcess::OnChorusParamChange)
    M_EVT_CHORUS_PITCH(wxID_ANY, CommandProcess::OnChorusParamChange)
    M_EVT_CHORUS_RATE(wxID_ANY, CommandProcess::OnChorusParamChange)
    M_EVT_CHORUS_TONE(wxID_ANY, CommandProcess::OnChorusParamChange)
#endif

#if PANEL_EN_DISTORTION
    M_EVT_DISTORTION(wxID_ANY, CommandProcess::OnDistortionParamChange)
    M_EVT_DISTORTION_ENABLE(wxID_ANY, CommandProcess::OnDistortionParamChange)
    M_EVT_DISTORTION_TONE(wxID_ANY, CommandProcess::OnDistortionParamChange)
    M_EVT_DISTORTION_DRIVE(wxID_ANY, CommandProcess::OnDistortionParamChange)
    M_EVT_DISTORTION_GAIN(wxID_ANY, CommandProcess::OnDistortionParamChange)
    M_EVT_DISTORTION_LEVEL(wxID_ANY, CommandProcess::OnDistortionParamChange)
    M_EVT_DISTORTION_FREQ(wxID_ANY, CommandProcess::OnDistortionParamChange)
    M_EVT_DISTORTION_WIDTH(wxID_ANY, CommandProcess::OnDistortionParamChange)
    M_EVT_DISTORTION_EQ(wxID_ANY, CommandProcess::OnDistortionParamChange)
#endif

#if PANEL_EN_COMPRESSOR
    M_EVT_COMPRESSOR(wxID_ANY, CommandProcess::OnCompressorParamChange)
    M_EVT_COMPRESSOR_ATTACK_TIME(wxID_ANY, CommandProcess::OnCompressorParamChange)
    M_EVT_COMPRESSOR_RELEASE_TIME(wxID_ANY, CommandProcess::OnCompressorParamChange)
    M_EVT_COMPRESSOR_THRESHOLD(wxID_ANY, CommandProcess::OnCompressorParamChange)
    M_EVT_COMPRESSOR_RATIO(wxID_ANY, CommandProcess::OnCompressorParamChange)
    M_EVT_COMPRESSOR_ENABLE(wxID_ANY, CommandProcess::OnCompressorParamChange)
    M_EVT_COMPRESSOR_POSTGAIN(wxID_ANY, CommandProcess::OnCompressorParamChange)

#endif

#if PANEL_EN_PRESET
    M_EVT_PRESET_INDEX(wxID_ANY,CommandProcess::OnPresetParamChange)
    M_EVT_PRESET_EFFECT_STATUS_UPDATE(wxID_ANY,CommandProcess::OnPresetParamChange)
#endif


wxEND_EVENT_TABLE()


CommandProcess::CommandProcess(USBCommunication *_usb_comm,char interface_num, unsigned char in_ep, unsigned char out_ep )
    :usb_comm(_usb_comm),usb_interface_num(interface_num),usb_in_ep(in_ep),usb_out_ep(out_ep)
{
    cmd_pack_max_len = 512;
    usb_in_ep |= 0x80;
    usb_out_ep &= 0x7F;

    Bind(myEVT_DISTORTION, &CommandProcess::OnDistortionParamChange, this, wxID_ANY);
}

CommandProcess::~CommandProcess()
{
}

CommandProcess::Status CommandProcess::WriteData(const char * data, int size)
{
    if(usb_comm->WriteData(data,size,usb_out_ep,300) == USBCommunication::SUCCESS)
    {
        return SUCCESS;
    }
    return FAIL;
}

CommandProcess::Status CommandProcess::ReadData(char * data, int *actual_length)
{
    if(usb_comm->ReadData(data,256,actual_length,usb_in_ep,500) == USBCommunication::SUCCESS)
    {
        return SUCCESS;
    }
    return FAIL;
}

int CommandProcess::create_command_packet(const char *data, int bytes,char *out_packet_buff)
{
    uint16_t len = bytes + 2;
    uint16_t verf_add ;
    uint8_t *data_buff;
    uint8_t *cmd_buf = (uint8_t *)out_packet_buff;
    int max_len;

    max_len = 4+bytes+2;
    if(max_len > cmd_pack_max_len)
    {
        return TOLONG;
    }
    if(data == NULL || out_packet_buff == NULL)
    {
        return FAIL;
    }

    data_buff = (uint8_t *)data;
    cmd_buf[0] = 0x55;
    cmd_buf[1] = 0xAA;
    cmd_buf[2] = (uint8_t)( len      & 0x00FF);
    cmd_buf[3] = (uint8_t)((len >> 8) & 0x00FF);
    verf_add  = cmd_buf[2] ;
    verf_add += cmd_buf[3] ;
    for(int i=0; i<bytes; i++)
    {
        verf_add += data_buff[i];
        cmd_buf[4+i] = data_buff[i];
    }
    cmd_buf[4+bytes+0] = (uint8_t)( verf_add       & 0x00FF);
    cmd_buf[4+bytes+1] = (uint8_t)((verf_add >> 8) & 0x00FF);
    // usb_device_bulk_send_data(cmd_buf,max_len);
    return max_len;
}


unsigned char CommandProcess::data_packet_analyze(const char *buff ,int len)
{
    // qDebug() <<"Fun:" << __func__ << ":thread ID:"<<QThread::currentThreadId();
    uint8_t command_recv_buffer[512];
    uint16_t analysis_steps;
    uint16_t data_bytes;
    uint16_t data_recv_index;
    uint16_t verf_add;
    uint16_t verf_recv;
    // const uint8_t *recv_buff = (uint8_t *)array.data();
    const uint16_t recv_length = len;
    uint16_t get_index=0;
    analysis_steps = 0;
    while(get_index < recv_length)
    {
        //uint8_t dat = recv_buff[get_index++];
        uint8_t dat = buff[get_index++];
        switch(analysis_steps)
        {
        case 0:
        {
            if(dat == 0x55)
            {
                analysis_steps = 1;
            }
        }
        break;
        case 1:
        {
            if(dat == 0xAA)
            {
                analysis_steps = 2;
                verf_add = 0;
            }
            else
            {
                analysis_steps = 0;
            }
        }
        break;
        case 2:
        {
            data_bytes = dat;
            analysis_steps = 3;
            verf_add += dat;
        }
        break;
        case 3:
        {
            data_bytes |= (uint16_t)dat << 8;
            if(data_bytes >= 2)
            {
                data_recv_index = 0;
                analysis_steps = 4;
                verf_add += dat;
            }
            else
            {
                analysis_steps = 0;
            }
        }
        break;
        case 4:
        {
            if(data_recv_index < (data_bytes-2))
            {
                verf_add += dat;
            }
            command_recv_buffer[data_recv_index] = dat;
            if(++data_recv_index >= data_bytes)
            {
                analysis_steps = 0;
                verf_recv  = command_recv_buffer[data_bytes-2];
                verf_recv |= (uint16_t)command_recv_buffer[data_bytes-1] << 8;
                if(verf_recv == verf_add)
                {
                    return command_data_analyze((const char *)command_recv_buffer,data_bytes-2);
                }
            }
        }
        break;
        default:
        {
            analysis_steps = 0;
        }
        break;
        }
    }
    return -1;
}


unsigned char CommandProcess::command_data_analyze(const char *data,int len)
{
    uint8_t cmd_index;
    uint8_t set_param_mark;
    uint8_t param_index;
    // uint8_t cmd_data_buffer[512];
    uint8_t *data_buff = (uint8_t *)data;

    cmd_index = data_buff[0];
    switch(cmd_index)
    {
        case CMD_INDEX_SYSTEM:
        {
            set_param_mark = (data_buff[1] & 0x80) ? 1 : 0;
            param_index = data_buff[1] & 0x7F;
            uint8_t *param_buffer = &data_buff[2];
            if(param_index == 0)
            {
                if(set_param_mark)
                {
                    uint8_t verf_hw;
                    uint8_t verf_main;
                    uint8_t verf_modify;
                    verf_hw = param_buffer[0];
                    verf_main = param_buffer[1];
                    verf_modify = param_buffer[2];
                    //t_dev_info *info = (t_dev_info *)data_struct;
                    param_struct_dev_info.hw_ver = verf_hw;
                    param_struct_dev_info.fw_ver_major = verf_main;
                    param_struct_dev_info.fw_ver_minor = verf_modify;
                    memcpy(param_struct_dev_info.name,&param_buffer[3],32);
                    return cmd_index;
                }
                else
                {
                }
            }
            else
            {

            }
        }
        break;
        case CMD_INDEX_EFFECT_REVERB :
        {
            uint32_t rev_enable;
            float rev_dry;
            float rev_wet;
            float rev_tone;
            float rev_damp;
            float rev_feedback;
            uint8_t preset_index;
            set_param_mark = (data_buff[1] & 0x80) ? 1 : 0;
            param_index = data_buff[1] & 0x7F;
            preset_index = data_buff[2];
            uint8_t *param_buffer = &data_buff[3];
            if(preset_index >= MAX_PRESET_NUM)
            {
                return -1;
            }
            if(param_index == EFFECT_PARAM_INDEX_REVERB)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    rev_enable = param_buffer[0] > 0 ? 1 : 0;
                    param  = param_buffer[1];
                    param |= (uint16_t)param_buffer[2] << 8;
                    rev_dry = param;
                    param  = param_buffer[3];
                    param |= (uint16_t)param_buffer[4] << 8;
                    rev_wet = param;
                    param  = param_buffer[5];
                    param |= (uint16_t)param_buffer[6] << 8;
                    rev_damp = param;
                    param  = param_buffer[7];
                    param |= (uint16_t)param_buffer[8] << 8;
                    rev_tone = param;
                    param  = param_buffer[9];
                    param |= (uint16_t)param_buffer[10] << 8;
                    rev_feedback = param;

                    param_struct_reverb.enable = rev_enable;
                    param_struct_reverb.dry = (float)rev_dry / PARAM_FLOAT_MUL;
                    param_struct_reverb.wet = (float)rev_wet / PARAM_FLOAT_MUL;
                    param_struct_reverb.damp = (float)rev_damp / PARAM_FLOAT_MUL;
                    param_struct_reverb.tone = (float)rev_tone / PARAM_FLOAT_MUL;
                    param_struct_reverb.feedback = (float)rev_feedback / PARAM_FLOAT_MUL;

                    return cmd_index;
                }
                else
                {
                    // int16_t params[10];

                    // rev_enable   = param_struct_reverb.enable;
                    // rev_dry      = param_struct_reverb.dry * PARAM_FLOAT_MUL;
                    // rev_wet      = param_struct_reverb.wet * PARAM_FLOAT_MUL;
                    // rev_damp     = param_struct_reverb.damp * PARAM_FLOAT_MUL;
                    // rev_tone     = param_struct_reverb.tone * PARAM_FLOAT_MUL;
                    // rev_feedback = param_struct_reverb.feedback * PARAM_FLOAT_MUL;


                    // params[0] = (int16_t)rev_enable;
                    // params[1] = (int16_t)rev_dry;
                    // params[2] = (int16_t)rev_wet;
                    // params[3] = (int16_t)rev_damp;
                    // params[4] = (int16_t)rev_tone;
                    // params[5] = (int16_t)rev_feedback;
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)params[0];
                    // command_send_buffer[4]  = (uint8_t)(params[1]      & 0x00FF);
                    // command_send_buffer[5]  = (uint8_t)(params[1] >> 8 & 0x00FF);
                    // command_send_buffer[6]  = (uint8_t)(params[2]      & 0x00FF);
                    // command_send_buffer[7]  = (uint8_t)(params[2] >> 8 & 0x00FF);
                    // command_send_buffer[8]  = (uint8_t)(params[3]      & 0x00FF);
                    // command_send_buffer[9]  = (uint8_t)(params[3] >> 8 & 0x00FF);
                    // command_send_buffer[10]  = (uint8_t)(params[4]      & 0x00FF);
                    // command_send_buffer[11]  = (uint8_t)(params[4] >> 8 & 0x00FF);
                    // command_send_buffer[12]  = (uint8_t)(params[5]      & 0x00FF);
                    // command_send_buffer[13] = (uint8_t)(params[5] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,14);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_REVERB_ENABLE)
            {
                if(set_param_mark)
                {
                    rev_enable = param_buffer[0] > 0 ? 1 : 0;

                    param_struct_reverb.enable = rev_enable;
                    return cmd_index;
                }
                else
                {
                    // rev_enable = param_struct_reverb.enable;
                    // command_send_buffer[0]  = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)rev_enable;
                    // command_send(command_send_buffer,3);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_REVERB_DRY)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    rev_dry = param;

                    param_struct_reverb.dry = (float)rev_dry / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // rev_dry      = param_struct_reverb.dry * PARAM_FLOAT_MUL;
                    // int16_t params[1];
                    // params[0] = (int16_t)rev_dry;
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,4);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_REVERB_WET)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    rev_wet = param;

                    param_struct_reverb.wet = (float)rev_wet / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // rev_wet      = param_struct_reverb.wet * PARAM_FLOAT_MUL;
                    // int16_t params[1];
                    // params[0] = (int16_t)rev_wet;
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,4);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_REVERB_DAMP)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    rev_damp = param;

                    param_struct_reverb.damp = (float)rev_damp / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // rev_damp     = param_struct_reverb.damp * PARAM_FLOAT_MUL;
                    // int16_t params[1];
                    // params[0] = (int16_t)rev_damp;
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,4);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_REVERB_TONE)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    rev_tone = param;

                    param_struct_reverb.tone = (float)rev_tone / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // rev_tone     = param_struct_reverb.tone * PARAM_FLOAT_MUL;
                    // int16_t params[1];
                    // params[0] = (int16_t)rev_tone;
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,4);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_REVERB_FEEDBACK)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    rev_feedback = param;

                    param_struct_reverb.feedback = (float)rev_feedback / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // int16_t params[1];
                    // rev_feedback = param_struct_reverb.feedback * PARAM_FLOAT_MUL;
                    // params[0] = (int16_t)rev_feedback;
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,4);
                }
            }
        }
        break;
        case CMD_INDEX_EFFECT_DELAY :
        {
            uint32_t dly_enable;
            float dly_feedback,dly_wet,dly_time_ms,dly_tone;
            uint8_t preset_index ;
            set_param_mark = (data_buff[1] & 0x80) ? 1 : 0;
            param_index = data_buff[1] & 0x7F;
            preset_index = data_buff[2];
            uint8_t *param_buffer = &data_buff[3];
            if(preset_index >= MAX_PRESET_NUM)
            {
                return -1;
            }
            if(param_index == EFFECT_PARAM_INDEX_DELAY)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param = param_buffer[0] > 0 ? 1 : 0;
                    dly_enable = param;
                    param  = param_buffer[1];
                    param |= (uint16_t)param_buffer[2] << 8;
                    dly_feedback = param;
                    param  = param_buffer[3];
                    param |= (uint16_t)param_buffer[4] << 8;
                    dly_wet = param;
                    param  = param_buffer[5];
                    param |= (uint16_t)param_buffer[6] << 8;
                    dly_time_ms = param;
                    param  = param_buffer[7];
                    param |= (uint16_t)param_buffer[8] << 8;
                    dly_tone = param;

                    param_struct_delay.enable = dly_enable;
                    param_struct_delay.feedback = (float)dly_feedback / PARAM_FLOAT_MUL;
                    param_struct_delay.wet      = (float)dly_wet / PARAM_FLOAT_MUL;
                    param_struct_delay.time_ms  = (float)dly_time_ms / PARAM_FLOAT_MUL;
                    param_struct_delay.tone     = (float)dly_tone / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // int16_t params[5];

                    // params[0] = param_struct_delay.enable;
                    // params[1] = param_struct_delay.feedback * PARAM_FLOAT_MUL;
                    // params[2] = param_struct_delay.wet * PARAM_FLOAT_MUL;
                    // params[3] = param_struct_delay.time;
                    // params[4] = param_struct_delay.tone * PARAM_FLOAT_MUL;

                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)params[0];
                    // command_send_buffer[4]  = (uint8_t)(params[1]      & 0x00FF);
                    // command_send_buffer[5]  = (uint8_t)(params[1] >> 8 & 0x00FF);
                    // command_send_buffer[6]  = (uint8_t)(params[2]      & 0x00FF);
                    // command_send_buffer[7]  = (uint8_t)(params[2] >> 8 & 0x00FF);
                    // command_send_buffer[8]  = (uint8_t)(params[3]      & 0x00FF);
                    // command_send_buffer[9]  = (uint8_t)(params[3] >> 8 & 0x00FF);
                    // command_send_buffer[10]  = (uint8_t)(params[4]      & 0x00FF);
                    // command_send_buffer[11]  = (uint8_t)(params[4] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,12);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_DELAY_ENABLE)
            {
                if(set_param_mark)
                {
                    dly_enable = param_buffer[0] > 0 ? 1 : 0;

                    param_struct_delay.enable   = dly_enable;
                    return cmd_index;
                }
                else
                {
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)param_struct_delay.enable;
                    // command_send(command_send_buffer,4);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_DELAY_FEEDBACK)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;

                    param_struct_delay.feedback = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // int16_t params[1];
                    // params[0] = (int16_t)(param_struct_delay.feedback * PARAM_FLOAT_MUL);
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,5);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_DELAY_WET)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;

                    param_struct_delay.wet = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // int16_t params[1];
                    // params[0] = (int16_t)(param_struct_delay.feedback * PARAM_FLOAT_MUL);
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,5);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_DELAY_TIME)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;

                    param_struct_delay.time_ms = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // int16_t params[1];
                    // params[0] = (int16_t)(param_struct_delay.time);
                    // command_send_buffer[0]  = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,5);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_DELAY_TONE)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;

                    param_struct_delay.tone = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // int16_t params[1];
                    // params[0] = (int16_t)(param_struct_delay.tone * PARAM_FLOAT_MUL);
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,5);
                }
            }
        }
        break;
        case CMD_INDEX_EFFECT_CHORUS:
        {
            float chr_dry;
            float chr_wet;
            float chr_pitch;
            float chr_rate;
            float chr_tone;
            uint32_t chr_enable;
            uint8_t preset_index ;
            set_param_mark = (data_buff[1] & 0x80) ? 1 : 0;
            param_index = data_buff[1] & 0x7F;
            preset_index = data_buff[2];
            uint8_t *param_buffer = &data_buff[3];
            if(preset_index >= MAX_PRESET_NUM)
            {
                return -1;
            }
            if(param_index == EFFECT_PARAM_INDEX_CHORUS)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param = param_buffer[0] > 0 ? 1 : 0;
                    chr_enable = param;
                    param  = param_buffer[1];
                    param |= (uint16_t)param_buffer[2] << 8;
                    chr_rate = param;
                    param = param_buffer[3];
                    param |= (uint16_t)param_buffer[4] << 8;
                    chr_pitch = param;
                    param  = param_buffer[5];
                    param |= (uint16_t)param_buffer[6] << 8;
                    chr_wet = param;
                    param  = param_buffer[7];
                    param |= (uint16_t)param_buffer[8] << 8;
                    chr_dry = param;
                    param  = param_buffer[9];
                    param |= (uint16_t)param_buffer[10] << 8;
                    chr_tone = param;

                    param_struct_chorus.enable = chr_enable;
                    param_struct_chorus.dry = (float)chr_dry / PARAM_FLOAT_MUL;
                    param_struct_chorus.wet = (float)chr_wet / PARAM_FLOAT_MUL;
                    param_struct_chorus.pitch = (float)chr_pitch / PARAM_FLOAT_MUL;
                    param_struct_chorus.rate = (float)chr_rate / PARAM_FLOAT_MUL;
                    param_struct_chorus.tone = (float)chr_tone / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // int16_t params[5];
                    // params[0] = (int16_t)param_struct_chorus.enable;
                    // params[1] = (int16_t)(param_struct_chorus.rate * PARAM_FLOAT_MUL);
                    // params[2] = (int16_t)(param_struct_chorus.pitch * PARAM_FLOAT_MUL);
                    // params[3] = (int16_t)(param_struct_chorus.wet * PARAM_FLOAT_MUL);
                    // params[4] = (int16_t)(param_struct_chorus.dry * PARAM_FLOAT_MUL);
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)params[0];
                    // command_send_buffer[4]  = (uint8_t)(params[1]      & 0x00FF);
                    // command_send_buffer[5]  = (uint8_t)(params[1] >> 8 & 0x00FF);
                    // command_send_buffer[6]  = (uint8_t)(params[2]      & 0x00FF);
                    // command_send_buffer[7]  = (uint8_t)(params[2] >> 8 & 0x00FF);
                    // command_send_buffer[8]  = (uint8_t)(params[3]      & 0x00FF);
                    // command_send_buffer[9]  = (uint8_t)(params[3] >> 8 & 0x00FF);
                    // command_send_buffer[10]  = (uint8_t)(params[4]      & 0x00FF);
                    // command_send_buffer[11]  = (uint8_t)(params[4] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,12);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_CHORUS_ENABLE)
            {
                int16_t param;
                if(set_param_mark)
                {
                    param = param_buffer[0] > 0 ? 1 : 0;

                    param_struct_chorus.enable = param;
                    return cmd_index;
                }
                else
                {
                    // param = param_struct_chorus.enable;
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)param;
                    // command_send(command_send_buffer,4);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_CHORUS_RATE)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;

                    param_struct_chorus.rate = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // int16_t params[1];
                    // params[0] = (int16_t)(param_struct_chorus.rate * PARAM_FLOAT_MUL);
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,5);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_CHORUS_PITCH)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;

                    param_struct_chorus.pitch = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // int16_t params[1];
                    // params[0] = (int16_t)(param_struct_chorus.pitch * PARAM_FLOAT_MUL);
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,5);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_CHORUS_WET)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;

                    param_struct_chorus.wet = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // int16_t params[1];
                    // params[0] = (int16_t)(param_struct_chorus.wet * PARAM_FLOAT_MUL);
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,5);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_CHORUS_DRY)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;

                    param_struct_chorus.dry = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // int16_t params[1];
                    // params[0] = (int16_t)(param_struct_chorus.dry * PARAM_FLOAT_MUL);
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,5);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_CHORUS_TONE)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;

                    param_struct_chorus.tone = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // int16_t params[1];
                    // params[0] = (int16_t)(param_struct_chorus.tone * PARAM_FLOAT_MUL);
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,5);
                }
            }
        }
        break;
        case CMD_INDEX_EFFECT_DISTORTION:
        {
            float dst_gain_in;
            float dst_gain_out;
            float dst_drive;
            float dst_tone;
            float dst_width;
            uint32_t dst_freq;
            uint32_t dst_bypass;
            uint8_t preset_index ;
            set_param_mark = (data_buff[1] & 0x80) ? 1 : 0;
            param_index = data_buff[1] & 0x7F;
            preset_index = data_buff[2];
            uint8_t *param_buffer = &data_buff[3];
            if(preset_index >= MAX_PRESET_NUM)
            {
                return 0;
            }
            if(param_index == EFFECT_PARAM_INDEX_DISTORTION)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param = param_buffer[0] > 0 ? 1 : 0;
                    dst_bypass = param;
                    param  = param_buffer[1];
                    param |= (uint16_t)param_buffer[2] << 8;
                    dst_gain_in = param;
                    param = param_buffer[3];
                    param |= (uint16_t)param_buffer[4] << 8;
                    dst_gain_out = param;
                    param  = param_buffer[5];
                    param |= (uint16_t)param_buffer[6] << 8;
                    dst_drive = param;
                    param  = param_buffer[7];
                    param |= (uint16_t)param_buffer[8] << 8;
                    dst_tone = param;
                    param  = param_buffer[9];
                    param |= (uint16_t)param_buffer[10] << 8;
                    dst_width = param;
                    param  = param_buffer[11];
                    param |= (uint16_t)param_buffer[12] << 8;
                    dst_freq = param;
                    param_struct_distortion.enable = dst_bypass;
                    param_struct_distortion.gain_input = (float)dst_gain_in / PARAM_FLOAT_MUL;
                    param_struct_distortion.gain_output = (float)dst_gain_out / PARAM_FLOAT_MUL;
                    param_struct_distortion.drive = (float)dst_drive / PARAM_FLOAT_MUL;
                    param_struct_distortion.tone = (float)dst_tone / PARAM_FLOAT_MUL;
                    param_struct_distortion.bandwidth = (float)dst_width / PARAM_FLOAT_MUL;
                    param_struct_distortion.freq = dst_freq;
                    return cmd_index;
                }
                else
                {

                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_DISTORTION_ENABLE)
            {
                int16_t param;
                if(set_param_mark)
                {
                    param = param_buffer[0] > 0 ? 1 : 0;
                    param_struct_distortion.enable = param;
                    return cmd_index;
                }
                else
                {
                    // param = distortion_param[preset_index].bypass;
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)param;
                    // command_send(command_send_buffer,4);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_DISTORTION_DRIVE)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    param_struct_distortion.drive = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // int16_t params[1];
                    // portDISABLE_INTERRUPTS();
                    // params[0] = (int16_t)(distortion_param[preset_index].drive * PARAM_FLOAT_MUL);
                    // portENABLE_INTERRUPTS();
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,5);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_DISTORTION_GAIN_INPUT)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    param_struct_distortion.gain_input = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // int16_t params[1];
                    // portDISABLE_INTERRUPTS();
                    // params[0] = (int16_t)(distortion_param[preset_index].gain_in * PARAM_FLOAT_MUL);
                    // portENABLE_INTERRUPTS();
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,5);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_DISTORTION_GAIN_OUTPUT)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    param_struct_distortion.gain_output = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // int16_t params[1];
                    // portDISABLE_INTERRUPTS();
                    // params[0] = (int16_t)(distortion_param[preset_index].gain_out * PARAM_FLOAT_MUL);
                    // portENABLE_INTERRUPTS();
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,5);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_DISTORTION_TONE)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    param_struct_distortion.tone = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                    // int16_t params[1];
                    // portDISABLE_INTERRUPTS();
                    // params[0] = (int16_t)(distortion_param[preset_index].tone * PARAM_FLOAT_MUL);
                    // portENABLE_INTERRUPTS();
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                    // command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,5);
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_DISTORTION_FREQ)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    param_struct_distortion.freq = param;
                    return cmd_index;
                }
                else
                {
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_DISTORTION_WIDGET)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    param_struct_distortion.bandwidth = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                else
                {
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_DISTORTION_EQ)
            {
                uint8_t band;
                uint8_t type;
                int16_t freq;
                int16_t gain;
                int16_t q;
                if(set_param_mark)
                {
                    int16_t param;
                    band  = param_buffer[0];
                    type  = param_buffer[1];;
                    param  = param_buffer[2];
                    param |= (uint16_t)param_buffer[3] << 8;
                    freq = param;
                    param  = param_buffer[4];
                    param |= (uint16_t)param_buffer[5] << 8;
                    gain = param;
                    param  = param_buffer[6];
                    param |= (uint16_t)param_buffer[7] << 8;
                    q = param;
                    param_struct_distortion.eq[band].type = (FilterType)type;
                    param_struct_distortion.eq[band].frequency = freq;
                    param_struct_distortion.eq[band].gain = (float)gain / PARAM_FLOAT_MUL;
                    param_struct_distortion.eq[band].qfactor = (float)q / PARAM_FLOAT_MUL;
                    return cmd_index;

                }
                // else
                // {
                //     int16_t params[6];
                //     band = param_buffer[0];
                //     if(band >= DISTORTION_EQ_MAX)
                //     {
                //         band = 0;
                //     }
                //     params[0] = band;
                //     params[1] = save_data.preset_params[preset_index].dist[sub_preset].eq[band].type ;
                //     params[2] = save_data.preset_params[preset_index].dist[sub_preset].eq[band].center_frequency ;
                //     params[3] = save_data.preset_params[preset_index].dist[sub_preset].eq[band].gain_db * PARAM_FLOAT_MUL;
                //     params[4] = save_data.preset_params[preset_index].dist[sub_preset].eq[band].q * PARAM_FLOAT_MUL;
                //     command_send_buffer[0] = cmd_index;
                //     command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                //     command_send_buffer[2]  = preset_index * DISTORTION_PRESET_NUM + sub_preset ;
                //     command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                //     command_send_buffer[4]  = (uint8_t)(params[1]      & 0x00FF);
                //     command_send_buffer[5]  = (uint8_t)(params[2]      & 0x00FF);
                //     command_send_buffer[6]  = (uint8_t)(params[2] >> 8 & 0x00FF);
                //     command_send_buffer[7]  = (uint8_t)(params[3]      & 0x00FF);
                //     command_send_buffer[8]  = (uint8_t)(params[3] >> 8 & 0x00FF);
                //     command_send_buffer[9]  = (uint8_t)(params[4]      & 0x00FF);
                //     command_send_buffer[10] = (uint8_t)(params[4] >> 8 & 0x00FF);
                //     command_send(command_send_buffer,11);
                // }
            }
        }
        break;
        case CMD_INDEX_EFFECT_WAH:
        {
            uint8_t wah_enable;
            uint8_t wah_speed;
            uint16_t wah_filter_freq_center;
            uint16_t wah_filter_freq_width;
            float wah_filter_gain;
            float wah_filter_q;
            uint8_t preset_index ;
            set_param_mark = (data_buff[1] & 0x80) ? 1 : 0;
            param_index = data_buff[1] & 0x7F;
            preset_index = data_buff[2];
            uint8_t *param_buffer = &data_buff[3];
            if(preset_index >= MAX_PRESET_NUM)
            {
                return 0;
            }
            if(param_index == EFFECT_PARAM_INDEX_WAH)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param = param_buffer[0] > 0 ? 1 : 0;
                    wah_enable = param;
                    param = param_buffer[1];
                    param |= (uint16_t)param_buffer[2] << 8;
                    wah_speed = param;
                    param = param_buffer[3];
                    param |= (uint16_t)param_buffer[4] << 8;
                    wah_filter_freq_center = param;
                    param  = param_buffer[5];
                    param |= (uint16_t)param_buffer[6] << 8;
                    wah_filter_freq_width = param;
                    param  = param_buffer[7];
                    param |= (uint16_t)param_buffer[8] << 8;
                    wah_filter_gain = param;
                    param  = param_buffer[9];
                    param |= (uint16_t)param_buffer[10] << 8;
                    wah_filter_q = param;
                    param_struct_wah.enable            = wah_enable;
                    param_struct_wah.speed             = wah_speed / PARAM_FLOAT_MUL;
                    param_struct_wah.filter_freq_center = wah_filter_freq_center;
                    param_struct_wah.filter_freq_width   = wah_filter_freq_width;
                    param_struct_wah.filter_gain       = wah_filter_gain / PARAM_FLOAT_MUL;
                    param_struct_wah.filter_q          = wah_filter_q / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                // else
                // {
                    // int16_t params[6];
                    // params[0] = (int16_t) save_data.preset_params[preset_index].wah_enable;
                    // params[1] = (int16_t)(save_data.preset_params[preset_index].wah_speed);
                    // params[2] = (int16_t)(save_data.preset_params[preset_index].wah_filter_freq_center);
                    // params[3] = (int16_t)(save_data.preset_params[preset_index].wah_filter_freq_width);
                    // params[4] = (int16_t)(save_data.preset_params[preset_index].wah_filter_gain * PARAM_FLOAT_MUL);
                    // params[5] = (int16_t)(save_data.preset_params[preset_index].wah_filter_q * PARAM_FLOAT_MUL);
                    // command_send_buffer[0] = cmd_index;
                    // command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                    // command_send_buffer[2]  = preset_index;
                    // command_send_buffer[3]  = (uint8_t)params[0];
                    // command_send_buffer[4]  = (uint8_t)params[1];
                    // command_send_buffer[5]  = (uint8_t)(params[2]      & 0x00FF);
                    // command_send_buffer[6]  = (uint8_t)(params[2] >> 8 & 0x00FF);
                    // command_send_buffer[7]  = (uint8_t)(params[3]      & 0x00FF);
                    // command_send_buffer[8]  = (uint8_t)(params[3] >> 8 & 0x00FF);
                    // command_send_buffer[9]  = (uint8_t)(params[4]      & 0x00FF);
                    // command_send_buffer[10]  = (uint8_t)(params[4] >> 8 & 0x00FF);
                    // command_send_buffer[11]  = (uint8_t)(params[5]      & 0x00FF);
                    // command_send_buffer[12]  = (uint8_t)(params[5] >> 8 & 0x00FF);
                    // command_send(command_send_buffer,13);
                // }
            }
            else if(param_index == EFFECT_PARAM_INDEX_WAH_ENABLE)
            {
                int16_t param;
                if(set_param_mark)
                {
                    param = param_buffer[0] > 0 ? 1 : 0;
                    param_struct_wah.enable = param;
                    return cmd_index;
                }
                // else
                // {
                //     portDISABLE_INTERRUPTS();
                //     param = save_data.preset_params[preset_index].wah_enable;
                //     portENABLE_INTERRUPTS();
                //     command_send_buffer[0] = cmd_index;
                //     command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                //     command_send_buffer[2]  = preset_index;
                //     command_send_buffer[3]  = (uint8_t)param;
                //     command_send(command_send_buffer,4);
                // }
            }
            else if(param_index == EFFECT_PARAM_INDEX_WAH_SPEED)
            {
                if(set_param_mark)
                {
                    uint16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    param_struct_wah.speed = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                // else
                // {
                //     uint8_t params[1];
                //     portDISABLE_INTERRUPTS();
                //     params[0] = save_data.preset_params[preset_index].wah_speed;
                //     portENABLE_INTERRUPTS();
                //     command_send_buffer[0] = cmd_index;
                //     command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                //     command_send_buffer[2]  = preset_index;
                //     command_send_buffer[3]  = (uint8_t)(params[0] & 0x00FF);
                //     command_send(command_send_buffer,4);
                // }
            }
            else if(param_index == EFFECT_PARAM_INDEX_WAH_FILTER_FREQ_CENTER)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    param_struct_wah.filter_freq_center = param;
                    return cmd_index;
                }
                // else
                // {
                //     int16_t params[1];
                //     portDISABLE_INTERRUPTS();
                //     params[0] = (int16_t)save_data.preset_params[preset_index].wah_filter_freq_center;
                //     portENABLE_INTERRUPTS();
                //     command_send_buffer[0] = cmd_index;
                //     command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                //     command_send_buffer[2]  = preset_index;
                //     command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                //     command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                //     command_send(command_send_buffer,5);
                // }
            }
            else if(param_index == EFFECT_PARAM_INDEX_WAH_FILTER_FREQ_WIDTH)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    param_struct_wah.filter_freq_width = param;
                    return cmd_index;
                }
                // else
                // {
                //     int16_t params[1];
                //     portDISABLE_INTERRUPTS();
                //     params[0] = (int16_t)save_data.preset_params[preset_index].wah_filter_freq_width;
                //     portENABLE_INTERRUPTS();
                //     command_send_buffer[0] = cmd_index;
                //     command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                //     command_send_buffer[2]  = preset_index;
                //     command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                //     command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                //     command_send(command_send_buffer,5);
                // }
            }
            else if(param_index == EFFECT_PARAM_INDEX_WAH_FILTER_GAIN)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    param_struct_wah.filter_gain = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                // else
                // {
                //     int16_t params[1];
                //     portDISABLE_INTERRUPTS();
                //     params[0] = (int16_t)(save_data.preset_params[preset_index].wah_filter_gain * PARAM_FLOAT_MUL);
                //     portENABLE_INTERRUPTS();
                //     command_send_buffer[0] = cmd_index;
                //     command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                //     command_send_buffer[2]  = preset_index;
                //     command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                //     command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                //     command_send(command_send_buffer,5);
                // }
            }
            else if(param_index == EFFECT_PARAM_INDEX_WAH_FILTER_Q)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    param_struct_wah.filter_q = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
                // else
                // {
                //     int16_t params[1];
                //     portDISABLE_INTERRUPTS();
                //     params[0] = (int16_t)(save_data.preset_params[preset_index].wah_filter_q * PARAM_FLOAT_MUL);
                //     portENABLE_INTERRUPTS();
                //     command_send_buffer[0] = cmd_index;
                //     command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
                //     command_send_buffer[2]  = preset_index;
                //     command_send_buffer[3]  = (uint8_t)(params[0]      & 0x00FF);
                //     command_send_buffer[4]  = (uint8_t)(params[0] >> 8 & 0x00FF);
                //     command_send(command_send_buffer,5);
                // }
            }
        }
        break;
        case CMD_INDEX_EFFECT_COMPRESSOR:
        {
            uint8_t cmp_enable;
            uint8_t cmp_threshold;
            uint16_t cmp_ratio;
            uint16_t cmp_attack_time;
            float cmp_release_time;
            float cmp_postgain;
            uint8_t preset_index ;
            set_param_mark = (data_buff[1] & 0x80) ? 1 : 0;
            param_index = data_buff[1] & 0x7F;
            preset_index = data_buff[2];
            uint8_t *param_buffer = &data_buff[3];
            if(preset_index >= MAX_PRESET_NUM)
            {
                return 0;
            }
            if(param_index == EFFECT_PARAM_INDEX_COMPRESSOR)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param = param_buffer[0] > 0 ? 1 : 0;
                    cmp_enable = param;
                    param = param_buffer[1];
                    param |= (uint16_t)param_buffer[2] << 8;
                    cmp_threshold = param;
                    param = param_buffer[3];
                    param |= (uint16_t)param_buffer[4] << 8;
                    cmp_ratio = param;
                    param  = param_buffer[5];
                    param |= (uint16_t)param_buffer[6] << 8;
                    cmp_attack_time = param;
                    param  = param_buffer[7];
                    param |= (uint16_t)param_buffer[8] << 8;
                    cmp_release_time = param;
                    param  = param_buffer[9];
                    param |= (uint16_t)param_buffer[10] << 8;
                    cmp_postgain = param;
                    param_struct_cmp.enable            = cmp_enable;
                    param_struct_cmp.threshold             = cmp_threshold / PARAM_FLOAT_MUL;
                    param_struct_cmp.ratio = cmp_ratio / PARAM_FLOAT_MUL;
                    param_struct_cmp.attack_time   = cmp_attack_time / PARAM_FLOAT_MUL;
                    param_struct_cmp.release_time       = cmp_release_time / PARAM_FLOAT_MUL;
                    param_struct_cmp.postgain          = cmp_postgain / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_COMPRESSOR_ENABLE)
            {
                int16_t param;
                if(set_param_mark)
                {
                    param = param_buffer[0] > 0 ? 1 : 0;
                    param_struct_cmp.enable = param;
                    return cmd_index;
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_COMPRESSOR_THRESHOLD)
            {
                if(set_param_mark)
                {
                    uint16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    param_struct_cmp.threshold = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_COMPRESSOR_RATIO)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    param_struct_cmp.ratio = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_COMPRESSOR_ATTACK_TIME)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    param_struct_cmp.attack_time = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_COMPRESSOR_RELEASE_TIME)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    param_struct_cmp.release_time = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_COMPRESSOR_POSTGAIN)
            {
                if(set_param_mark)
                {
                    int16_t param;
                    param  = param_buffer[0];
                    param |= (uint16_t)param_buffer[1] << 8;
                    param_struct_cmp.postgain = (float)param / PARAM_FLOAT_MUL;
                    return cmd_index;
                }
            }
        }
        break;
        case CMD_INDEX_EFFECT_PRESET:
        {
            uint8_t preset_index;
            set_param_mark = (data_buff[1] & 0x80) ? 1 : 0;
            param_index = data_buff[1] & 0x7F;
            preset_index = data_buff[2];
            uint8_t *param_buffer = &data_buff[3];
            if(param_index == EFFECT_PARAM_INDEX_PRESET_SAVE)
            {
                if(set_param_mark)
                {
                    if(preset_index == 0xFF)
                    {
                    }
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_PRESET_CHANGE)
            {
                if(set_param_mark)
                {
                    //切换preset
                    if(preset_index < MAX_PRESET_NUM)
                    {
                        param_struct_preset.preset_index = preset_index;
                        return cmd_index;
                    }
                }
            }
            else if(param_index == EFFECT_PARAM_INDEX_PRESET_SET_GROUP)
            {
                if(set_param_mark)
                {
                    uint32_t mark = 0;
                    mark <<= 8;
                    mark |= param_buffer[3];
                    mark <<= 8;
                    mark |= param_buffer[2];
                    mark <<= 8;
                    mark |= param_buffer[1];
                    mark <<= 8;
                    mark |= param_buffer[0];
                    param_struct_preset.effect_status_mark = mark;
                    return cmd_index;
                }
            }
        }
        break;
    }
    return -1;
}

CommandProcess::Status CommandProcess::save_param_to_device(void)
{
    uint8_t param_buff[3];
    uint8_t cmd_pack_buff[512];

    param_buff[0] = CMD_INDEX_EFFECT_PRESET;
    param_buff[1] = 0x80 | EFFECT_PARAM_INDEX_PRESET_SAVE;
    param_buff[2] = 0xFF;
    int ret = create_command_packet((const char *)param_buff,3,(char *)cmd_pack_buff);
    return WriteData((char *)cmd_pack_buff,ret);
}

CommandProcess::Status CommandProcess::get_device_info(t_dev_info &info)
{
    uint8_t param_buff[3];
    uint8_t cmd_pack_buff[512];
    int actual_length;

    param_buff[0]=CMD_INDEX_SYSTEM;
    param_buff[1]=(char)0;
    int ret = create_command_packet((const char *)param_buff,2,(char *)cmd_pack_buff);
    if(WriteData((char *)cmd_pack_buff,ret) == SUCCESS)
    {
        //每条命令的长度不一样
        if(ReadData((char *)cmd_pack_buff,&actual_length) == SUCCESS)
        {
            int cmd;
            cmd = data_packet_analyze((const char *)cmd_pack_buff,512);
            if(cmd == param_buff[0])
            {
                memcpy((void *)&info,(void *)&param_struct_dev_info,(int)sizeof(t_dev_info));
                return SUCCESS;
            }
        }
        return FAIL;
    }
    return FAIL;
}


CommandProcess::Status CommandProcess::get_param_reverb(int preset_num,PanelReverb::param_struct &param)
{
    uint8_t param_buff[3];
    uint8_t cmd_pack_buff[512];
    int actual_length;

    param_buff[0]=CMD_INDEX_EFFECT_REVERB;
    param_buff[1]=(char)EFFECT_PARAM_INDEX_REVERB | 0x00;
    param_buff[2]=(char)preset_num;
    int ret = create_command_packet((const char *)param_buff,3,(char *)cmd_pack_buff);
    if(WriteData((char *)cmd_pack_buff,ret) == SUCCESS)
    {
        if(ReadData((char *)cmd_pack_buff,&actual_length) == SUCCESS)
        {
            int cmd;
            cmd = data_packet_analyze((const char *)cmd_pack_buff,512);
            if(cmd == param_buff[0])
            {
                memcpy((void *)&param,(void *)&param_struct_reverb,(int)sizeof(PanelReverb::param_struct));
                return SUCCESS;
            }
        }
        return FAIL;
    }
    return FAIL;
}

CommandProcess::Status CommandProcess::get_param_chorus(int preset_num,PanelChorus::param_struct &param)
{
    uint8_t param_buff[3];
    uint8_t cmd_pack_buff[512];
    int actual_length;

    param_buff[0]=CMD_INDEX_EFFECT_CHORUS;
    param_buff[1]=(char)0 | 0x00;
    param_buff[2]=(char)preset_num;
    int ret = create_command_packet((const char *)param_buff,3,(char *)cmd_pack_buff);
    if(WriteData((char *)cmd_pack_buff,ret) == SUCCESS)
    {
        if(ReadData((char *)cmd_pack_buff,&actual_length) == SUCCESS)
        {
            int cmd;
            cmd = data_packet_analyze((const char *)cmd_pack_buff,512);
            if(cmd == param_buff[0])
            {
                memcpy((void *)&param,(void *)&param_struct_chorus,(int)sizeof(PanelChorus::param_struct));
                return SUCCESS;
            }
        }
        return FAIL;
    }
    return FAIL;
}

CommandProcess::Status CommandProcess::get_param_delay(int preset_num,PanelDelay::param_struct &param)
{
    uint8_t param_buff[3];
    uint8_t cmd_pack_buff[512];
    int actual_length;

    param_buff[0]=CMD_INDEX_EFFECT_DELAY;
    param_buff[1]=(char)0 | 0x00;
    param_buff[2]=(char)preset_num;
    int ret = create_command_packet((const char *)param_buff,3,(char *)cmd_pack_buff);
    if(WriteData((char *)cmd_pack_buff,ret) == SUCCESS)
    {
        if(ReadData((char *)cmd_pack_buff,&actual_length) == SUCCESS)
        {
            int cmd;
            cmd = data_packet_analyze((const char *)cmd_pack_buff,512);
            if(cmd == param_buff[0])
            {
                memcpy((void *)&param,(void *)&param_struct_delay,(int)sizeof(PanelDelay::param_struct));
                return SUCCESS;
            }
        }
        return FAIL;
    }
    return FAIL;
}

CommandProcess::Status CommandProcess::get_param_wah(int preset_num,PanelWah::param_struct &param)
{
    uint8_t param_buff[3];
    uint8_t cmd_pack_buff[512];
    int actual_length;

    param_buff[0]=CMD_INDEX_EFFECT_WAH;
    param_buff[1]=(char)0 | 0x00;
    param_buff[2]=(char)preset_num;
    int ret = create_command_packet((const char *)param_buff,3,(char *)cmd_pack_buff);
    if(WriteData((char *)cmd_pack_buff,ret) == SUCCESS)
    {
        if(ReadData((char *)cmd_pack_buff,&actual_length) == SUCCESS)
        {
            int cmd;
            cmd = data_packet_analyze((const char *)cmd_pack_buff,512);
            if(cmd == param_buff[0])
            {
                memcpy((void *)&param,(void *)&param_struct_wah,(int)sizeof(PanelWah::param_struct));
                return SUCCESS;
            }
        }
        return FAIL;
    }
    return FAIL;
}

CommandProcess::Status CommandProcess::get_param_compressor(int preset_num,PanelCompressor::param_struct &param)
{
    uint8_t param_buff[3];
    uint8_t cmd_pack_buff[512];
    int actual_length;

    param_buff[0]=CMD_INDEX_EFFECT_COMPRESSOR;
    param_buff[1]=(char)0 | 0x00;
    param_buff[2]=(char)preset_num;
    int ret = create_command_packet((const char *)param_buff,3,(char *)cmd_pack_buff);
    if(WriteData((char *)cmd_pack_buff,ret) == SUCCESS)
    {
        if(ReadData((char *)cmd_pack_buff,&actual_length) == SUCCESS)
        {
            int cmd;
            cmd = data_packet_analyze((const char *)cmd_pack_buff,512);
            if(cmd == param_buff[0])
            {
                memcpy((void *)&param,(void *)&param_struct_cmp,(int)sizeof(PanelCompressor::param_struct));
                return SUCCESS;
            }
        }
        return FAIL;
    }
    return FAIL;
}


CommandProcess::Status CommandProcess::get_param_distortion(int preset_num,PanelDistortion::param_struct &param)
{
    uint8_t param_buff[3];
    uint8_t cmd_pack_buff[512];
    int actual_length;

    param_buff[0]=CMD_INDEX_EFFECT_DISTORTION;
    param_buff[1]=(char)0 | 0x00;
    param_buff[2]=(char)preset_num;
    int ret = create_command_packet((const char *)param_buff,3,(char *)cmd_pack_buff);
    if(WriteData((char *)cmd_pack_buff,ret) == SUCCESS)
    {
        if(ReadData((char *)cmd_pack_buff,&actual_length) == SUCCESS)
        {
            int cmd;
            cmd = data_packet_analyze((const char *)cmd_pack_buff,512);
            if(cmd == param_buff[0])
            {
                memcpy((void *)&param,(void *)&param_struct_distortion,(int)sizeof(PanelDistortion::param_struct));
                return SUCCESS;
            }
        }
        return FAIL;
    }
    return FAIL;
}

CommandProcess::Status CommandProcess::get_param_distortion_eq(int preset_num,int band,PanelDistortion::param_struct &param)
{
    uint8_t param_buff[4];
    uint8_t cmd_pack_buff[512];
    int actual_length;

    param_buff[0]=CMD_INDEX_EFFECT_DISTORTION;
    param_buff[1]=(char)EFFECT_PARAM_INDEX_DISTORTION_EQ | 0x00;
    param_buff[2]=(char)preset_num;
    param_buff[3]=(char)band;
    int ret = create_command_packet((const char *)param_buff,4,(char *)cmd_pack_buff);
    if(WriteData((char *)cmd_pack_buff,ret) == SUCCESS)
    {
        if(ReadData((char *)cmd_pack_buff,&actual_length) == SUCCESS)
        {
            int cmd;
            cmd = data_packet_analyze((const char *)cmd_pack_buff,512);
            if(cmd == param_buff[0])
            {
                memcpy((void *)&param,(void *)&param_struct_distortion,(int)sizeof(PanelDistortion::param_struct));
                return SUCCESS;
            }
        }
        return FAIL;
    }
    return FAIL;
}

CommandProcess::Status CommandProcess::send_get_param_command_data(int preset_num,uint8_t cmd,uint8_t param_index,uint8_t *data,int len)
{
    uint8_t command_send_buffer[512];
    uint8_t cmd_pack_buff[512];

    command_send_buffer[0]  = cmd;
    command_send_buffer[1]  = (uint8_t)(0x80 | param_index);
    command_send_buffer[2]  = preset_num;
    if(data)
    {
        memcpy((void *)&command_send_buffer[3],(void *)data,(int)len);
    }
    else
    {
        len = 0;
    }

    int ret = create_command_packet((const char *)command_send_buffer,3+len,(char *)cmd_pack_buff);
    if(WriteData((char *)cmd_pack_buff,ret) == SUCCESS)
    {
        return SUCCESS;
    }
    return FAIL;

}


void CommandProcess::OnReverbParamChange(MyEventReverb &event)
{
#if PANEL_EN_REVERB
    wxEventType type = event.GetEventType();
    int index = event.GetPresetIndex();
    if(type == myEVT_REVERB_ENABLE)
    {
        bool enable = event.GetEnable();
        set_param_reverb_enable(index,enable);
        wxLogInfo(wxString::Format("reverb enable: %d",enable));
    }
    else if(type == myEVT_REVERB_DAMPING)
    {
        double value = event.GetDamping();
        set_param_reverb_damp(index,value);
        wxLogInfo(wxString::Format("reverb damping: %f",value));
    }
    else if(type == myEVT_REVERB_FEEDBACK)
    {
        double value = event.GetFeedback();
        set_param_reverb_feedback(index,value);
        wxLogInfo(wxString::Format("reverb feedback: %f",value));
    }
    else if(type == myEVT_REVERB_TONE)
    {
        double value = event.GetTone();
        set_param_reverb_tone(index,value);
        wxLogInfo(wxString::Format("reverb tone: %f",value));
    }
    else if(type == myEVT_REVERB_WET)
    {
        double value = event.GetWet();
        set_param_reverb_wet(index,value);
        wxLogInfo(wxString::Format("reverb wet: %f",value));
    }
    else if(type == myEVT_REVERB_DRY)
    {
        double value = event.GetDry();
        set_param_reverb_dry(index,value);
        wxLogInfo(wxString::Format("reverb dry: %f",value));
    }
    else if(type == myEVT_REVERB)
    {
        PanelReverb::param_struct param;
        param.dry = event.GetDry();
        param.wet = event.GetWet();
        param.tone = event.GetTone();
        param.feedback = event.GetFeedback();
        param.damp = event.GetDamping();
        param.enable = event.GetEnable();
        set_param_reverb(index,param);
        wxLogInfo("reverb all update");
    }

#endif
    // event.Skip();
}

void CommandProcess::OnChorusParamChange(MyEventChorus &event)
{
#if PANEL_EN_CHORUS
    wxEventType type = event.GetEventType();
    int index = event.GetPresetIndex();
    if(type == myEVT_CHORUS_ENABLE)
    {
        bool enable = event.GetEnable();
        set_param_chorus_enable(index,enable);
        wxLogInfo(wxString::Format("chorus enable: %d",enable));
    }
    else if(type == myEVT_CHORUS_DRY)
    {
        double value = event.GetDry();
        set_param_chorus_dry(index,value);
        wxLogInfo(wxString::Format("chorus dry: %f",value));
    }
    else if(type == myEVT_CHORUS_WET)
    {
        double value = event.GetWet();
        set_param_chorus_wet(index,value);
        wxLogInfo(wxString::Format("chorus wet: %f",value));
    }
    else if(type == myEVT_CHORUS_PITCH)
    {
        double value = event.GetPitch();
        set_param_chorus_pitch(index,value);
        wxLogInfo(wxString::Format("chorus pitch: %f",value));
    }
    else if(type == myEVT_CHORUS_RATE)
    {
        double value = event.GetRate();
        set_param_chorus_rate(index,value);
        wxLogInfo(wxString::Format("chorus rate: %f",value));
    }
    else if(type == myEVT_CHORUS_TONE)
    {
        double value = event.GetTone();
        set_param_chorus_tone(index,value);
        wxLogInfo(wxString::Format("chorus tone: %f",value));
    }
    else if(type == myEVT_CHORUS)
    {
        PanelChorus::param_struct param;
        param.enable = event.GetEnable();
        param.dry = event.GetDry();
        param.wet = event.GetWet();
        param.pitch = event.GetPitch();
        param.rate = event.GetRate();
        param.tone = event.GetTone();
        set_param_chorus(index,param);
        wxLogInfo("chorus all update");
    }
#endif
    // event.Skip();
}
void CommandProcess::OnDelayParamChange(MyEventDelay &event)
{
#if PANEL_EN_DELAY
    wxEventType type = event.GetEventType();
    int index = event.GetPresetIndex();
    if(type == myEVT_DELAY_ENABLE)
    {
        bool enable = event.GetEnable();
        set_param_delay_enable(index,enable);
        wxLogInfo(wxString::Format("delay enable: %d",enable));
    }
    else if(type == myEVT_DELAY_TIME)
    {
        double value = event.GetTime();
        set_param_delay_time(index,value);
        wxLogInfo(wxString::Format("delay time: %f",value));
    }
    else if(type == myEVT_DELAY_FEEDBACK)
    {
        double value = event.GetFeedback();
        set_param_delay_feedback(index,value);
        wxLogInfo(wxString::Format("delay feedback: %f",value));
    }
    else if(type == myEVT_DELAY_WET)
    {
        double value = event.GetWet();
        set_param_delay_wet(index,value);
        wxLogInfo(wxString::Format("delay wet: %f",value));
    }
    else if(type == myEVT_DELAY_TONE)
    {
        double value = event.GetTone();
        set_param_delay_tone(index,value);
        wxLogInfo(wxString::Format("delay tone: %f",value));
    }
    else if(type == myEVT_DELAY)
    {
        PanelDelay::param_struct param;
        param.enable = event.GetEnable();
        param.time_ms = event.GetTime();
        param.feedback = event.GetFeedback();
        param.wet = event.GetWet();
        param.tone = event.GetTone();
        set_param_delay(index,param);
        wxLogInfo("delay all update");
    }
#endif
    // event.Skip();
}

void CommandProcess::OnWahParamChange(MyEventWah &event)
{
#if PANEL_EN_WAH
    wxEventType type = event.GetEventType();
    int index = event.GetPresetIndex();
    if(type == myEVT_WAH_ENABLE)
    {
        bool enable = event.GetEnable();
        set_param_wah_enable(index,enable);
        wxLogInfo(wxString::Format("compressor enable: %d",enable));
    }
    else if(type == myEVT_WAH_SPEED)
    {
        double value = event.GetSpeed();
        set_param_wah_speed(index,value);
        wxLogInfo(wxString::Format("wah speed: %f",value));
    }
    else if(type == myEVT_WAH_FILTER_FREQ_WIDTH)
    {
        double value = event.GetFilterFreqWidth();
        set_param_wah_filter_freq_width(index,value);
        wxLogInfo(wxString::Format("wah filter freq end: %f",value));
    }
    else if(type == myEVT_WAH_FILTER_FREQ_CENTER)
    {
        double value = event.GetFilterFreqCenter();
        set_param_wah_filter_freq_center(index,value);
        wxLogInfo(wxString::Format("wah filter freq start: %f",value));
    }
    else if(type == myEVT_WAH_FILTER_GAIN)
    {
        double value = event.GetFilterGain();
        set_param_wah_filter_gain(index,value);
        wxLogInfo(wxString::Format("wah filter gain: %f",value));
    }
    else if(type == myEVT_WAH_FILTER_Q)
    {
        double value = event.GetFilterQ();
        set_param_wah_filter_q(index,value);
        wxLogInfo(wxString::Format("wah filter q: %f",value));
    }
    else if(type == myEVT_WAH)
    {
        PanelWah::param_struct param;
        param.enable = event.GetEnable();
        param.speed = event.GetSpeed();
        param.filter_freq_center = event.GetFilterFreqCenter();
        param.filter_freq_width = event.GetFilterFreqWidth();
        param.filter_gain = event.GetFilterGain();
        param.filter_q = event.GetFilterQ();
        set_param_wah(index,param);
        wxLogInfo("wah all update");
    }
#endif
    // event.Skip();
}

void CommandProcess::OnDistortionParamChange(MyEventDistortion &event)
{
#if PANEL_EN_DISTORTION
    wxEventType type = event.GetEventType();
    int index = event.GetPresetIndex();
    if(type == myEVT_DISTORTION_ENABLE)
    {
        bool enable = event.GetEnable();
        set_param_distortion_enable(index,enable);
        wxLogInfo(wxString::Format("distortion enable: %d",enable));
    }
    else if(type == myEVT_DISTORTION_GAIN)
    {
        double value = event.GetGain();
        set_param_distortion_gain_input(index,value);
        wxLogInfo(wxString::Format("distortion gain: %f",value));
    }
    else if(type == myEVT_DISTORTION_DRIVE)
    {
        double value = event.GetDrive();
        set_param_distortion_drive(index,value);
        wxLogInfo(wxString::Format("distortion drive: %f",value));
    }
    else if(type == myEVT_DISTORTION_LEVEL)
    {
        double value = event.GetLevel();
        set_param_distortion_gain_output(index,value);
        wxLogInfo(wxString::Format("distortion level: %f",value));
    }
    else if(type == myEVT_DISTORTION_TONE)
    {
        double value = event.GetTone();
        set_param_distortion_tone(index,value);
        wxLogInfo(wxString::Format("distortion tone: %f",value));
    }
    else if(type == myEVT_DISTORTION_FREQ)
    {
        double value = event.GetFreq();
        set_param_distortion_freq(index,value);
        wxLogInfo(wxString::Format("distortion freq: %f",value));
    }
    else if(type == myEVT_DISTORTION_WIDTH)
    {
        double value = event.GetWidth();
        set_param_distortion_width(index,value);
        wxLogInfo(wxString::Format("distortion bandwidth: %f",value));
    }
    else if(type == myEVT_DISTORTION_EQ)
    {
        int band;
        FilterParam param;
        band = event.GetEqualizerParam(param);
        set_param_distortion_eq(index,band,param);
        wxLogInfo(wxString::Format("distortion eq,type:%d,freq:%f,gain:%f,q:%f",param.type,param.frequency,param.gain,param.qfactor));
    }
    else if(type == myEVT_DISTORTION)
    {
        PanelDistortion::param_struct param;
        param.enable = event.GetEnable();
        param.gain_input = event.GetGain();
        param.drive = event.GetDrive();
        param.gain_output = event.GetLevel();
        param.tone = event.GetTone();
        param.freq = event.GetFreq();
        param.bandwidth = event.GetWidth();
        set_param_distortion(index,param);

        int band;
        FilterParam param_eq;
        band = event.GetEqualizerParam(param_eq);
        set_param_distortion_eq(index,band,param_eq);

        wxLogInfo("distortion all update");
    }
#endif
    // event.Skip();
}
void CommandProcess::OnCompressorParamChange(MyEventCompressor &event)
{
#if PANEL_EN_COMPRESSOR
    wxEventType type = event.GetEventType();
    int index = event.GetPresetIndex();
    if(type == myEVT_COMPRESSOR_ENABLE)
    {
        bool enable = event.GetEnable();
        set_param_compressor_enable(index,enable);
        wxLogInfo(wxString::Format("compressor enable: %d",enable));
    }
    else if(type == myEVT_COMPRESSOR_THRESHOLD)
    {
        double value = event.GetThreshold();
        set_param_compressor_threshold(index,value);
        wxLogInfo(wxString::Format("compressor threshold: %f",value));
    }
    else if(type == myEVT_COMPRESSOR_RATIO)
    {
        double value = event.GetRatio();
        set_param_compressor_ratio(index,value);
        wxLogInfo(wxString::Format("compressor filter freq end: %f",value));
    }
    else if(type == myEVT_COMPRESSOR_ATTACK_TIME)
    {
        double value = event.GetAttackTime();
        set_param_compressor_attack_time(index,value);
        wxLogInfo(wxString::Format("compressor filter freq start: %f",value));
    }
    else if(type == myEVT_COMPRESSOR_RELEASE_TIME)
    {
        double value = event.GetReleaseTime();
        set_param_compressor_release_time(index,value);
        wxLogInfo(wxString::Format("compressor filter gain: %f",value));
    }
    else if(type == myEVT_COMPRESSOR_POSTGAIN)
    {
        double value = event.GetPostGain();
        set_param_compressor_postgain(index,value);
        wxLogInfo(wxString::Format("compressor filter q: %f",value));
    }
    else if(type == myEVT_COMPRESSOR)
    {
        PanelCompressor::param_struct param;
        param.enable = event.GetEnable();
        param.threshold = event.GetThreshold();
        param.attack_time = event.GetAttackTime();
        param.ratio = event.GetRatio();
        param.release_time = event.GetReleaseTime();
        param.postgain = event.GetPostGain();
        set_param_compressor(index,param);
        wxLogInfo("compressor all update");
    }
#endif
    // event.Skip();
}

void CommandProcess::OnEqualizerParamChange(MyEventEqualizer &event)
{
    // event.Skip();
}

void CommandProcess::OnPresetParamChange(MyEventPreset &event)
{
    wxEventType type = event.GetEventType();
    int index = event.GetPresetIndex();
    if(type == myEVT_PRESET_INDEX)
    {
        int value = event.GetPresetIndex();
        set_param_preset_index(value);
        wxLogInfo(wxString::Format("preset change: %d",value));
    }
    else if(type == myEVT_PRESET_EFFECT_STATUS_UPDATE)
    {
        int index = event.GetPresetIndex();
        int s_rev = event.GetEffectStatusReverb();
        int s_dist1 = event.GetEffectStatusDistortion1();
        int s_dist2 = event.GetEffectStatusDistortion2();
        int s_delay = event.GetEffectStatusDelay();
        int s_wah = event.GetEffectStatusWah();
        int s_chorus = event.GetEffectStatusChorus();
        int mark = 0;
        mark |= s_rev ? (1ul << EFFECT_INDEX_REVERB) : 0;
        mark |= s_chorus ? (1ul << EFFECT_INDEX_CHORUS) : 0;
        mark |= s_delay ? (1ul << EFFECT_INDEX_DELAY) : 0;
        mark |= s_wah ? (1ul << EFFECT_INDEX_WAH) : 0;
        mark |= s_dist1 ? (1ul << EFFECT_INDEX_DISTORTION1) : 0;
        mark |= s_dist2 ? (1ul << EFFECT_INDEX_DISTORTION2) : 0;

        set_param_preset_group(index,mark);
        wxLogInfo(wxString::Format("preset group status: 0x%x",mark));
    }
    // event.Skip();
}

CommandProcess::Status CommandProcess::set_param_reverb(int preset_num,PanelReverb::param_struct &param)
{
    uint8_t command_send_buffer[16];
    int16_t params[10];

    param_struct_reverb.enable = param.enable;
    param_struct_reverb.dry = param.dry;
    param_struct_reverb.wet = param.wet;
    param_struct_reverb.damp = param.damp;
    param_struct_reverb.tone = param.tone;
    param_struct_reverb.feedback = param.feedback;

    params[0]               = (int16_t)param.enable;
    params[1]               = (int16_t)(param.dry * PARAM_FLOAT_MUL);
    params[2]               = (int16_t)(param.wet * PARAM_FLOAT_MUL);
    params[3]               = (int16_t)(param.damp * PARAM_FLOAT_MUL);
    params[4]               = (int16_t)(param.tone * PARAM_FLOAT_MUL);
    params[5]               = (int16_t)(param.feedback * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)params[0];
    command_send_buffer[1]  = (uint8_t)(params[1]        & 0x00FF);
    command_send_buffer[2]  = (uint8_t)((params[1] >> 8) & 0x00FF);
    command_send_buffer[3]  = (uint8_t)(params[2]        & 0x00FF);
    command_send_buffer[4]  = (uint8_t)((params[2] >> 8) & 0x00FF);
    command_send_buffer[5]  = (uint8_t)(params[3]        & 0x00FF);
    command_send_buffer[6]  = (uint8_t)((params[3] >> 8) & 0x00FF);
    command_send_buffer[7]  = (uint8_t)(params[4]        & 0x00FF);
    command_send_buffer[8]  = (uint8_t)((params[4] >> 8) & 0x00FF);
    command_send_buffer[9]  = (uint8_t)(params[5]        & 0x00FF);
    command_send_buffer[10] = (uint8_t)((params[5] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_REVERB,EFFECT_PARAM_INDEX_REVERB,command_send_buffer,11);
}

CommandProcess::Status CommandProcess::set_param_reverb_damp(int preset_num,double damp)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_reverb.damp = damp;

    params[0]               = (int16_t)(damp * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_REVERB,EFFECT_PARAM_INDEX_REVERB_DAMP,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_reverb_dry(int preset_num,double dry)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_reverb.dry = dry;

    params[0]               = (int16_t)(dry * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_REVERB,EFFECT_PARAM_INDEX_REVERB_DRY,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_reverb_wet(int preset_num,double wet)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_reverb.wet = wet;

    params[0]               = (int16_t)(wet * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_REVERB,EFFECT_PARAM_INDEX_REVERB_WET,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_reverb_feedback(int preset_num,double feedback)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_reverb.feedback = feedback;

    params[0]               = (int16_t)(feedback * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_REVERB,EFFECT_PARAM_INDEX_REVERB_FEEDBACK,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_reverb_tone(int preset_num,double tone)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_reverb.tone = tone;

    params[0]               = (int16_t)(tone * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_REVERB,EFFECT_PARAM_INDEX_REVERB_TONE,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_reverb_enable(int preset_num,bool enable)
{
    uint8_t command_send_buffer[16];

    param_struct_reverb.enable = enable;

    command_send_buffer[0]  = (uint8_t)(enable ? 1 : 0);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_REVERB,EFFECT_PARAM_INDEX_REVERB_ENABLE,command_send_buffer,1);
}

CommandProcess::Status CommandProcess::set_param_chorus(int preset_num,PanelChorus::param_struct &param)
{
    uint8_t command_send_buffer[16];
    int16_t params[10];

    param_struct_chorus.enable = param.enable;
    param_struct_chorus.dry = param.dry;
    param_struct_chorus.wet = param.wet;
    param_struct_chorus.pitch = param.pitch;
    param_struct_chorus.rate = param.rate;
    param_struct_chorus.tone = param.tone;

    params[0]               = (int16_t)param.enable;
    params[1]               = (int16_t)(param.dry * PARAM_FLOAT_MUL);
    params[2]               = (int16_t)(param.wet * PARAM_FLOAT_MUL);
    params[3]               = (int16_t)(param.pitch * PARAM_FLOAT_MUL);
    params[4]               = (int16_t)(param.rate * PARAM_FLOAT_MUL);
    params[5]               = (int16_t)(param.tone * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)params[0];
    command_send_buffer[1]  = (uint8_t)(params[1]        & 0x00FF);
    command_send_buffer[2]  = (uint8_t)((params[1] >> 8) & 0x00FF);
    command_send_buffer[3]  = (uint8_t)(params[2]        & 0x00FF);
    command_send_buffer[4]  = (uint8_t)((params[2] >> 8) & 0x00FF);
    command_send_buffer[5]  = (uint8_t)(params[3]        & 0x00FF);
    command_send_buffer[6]  = (uint8_t)((params[3] >> 8) & 0x00FF);
    command_send_buffer[7]  = (uint8_t)(params[4]        & 0x00FF);
    command_send_buffer[8]  = (uint8_t)((params[4] >> 8) & 0x00FF);
    command_send_buffer[9]  = (uint8_t)(params[5]        & 0x00FF);
    command_send_buffer[10]  = (uint8_t)((params[5] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_CHORUS,EFFECT_PARAM_INDEX_CHORUS,command_send_buffer,11);
}

CommandProcess::Status CommandProcess::set_param_chorus_enable(int preset_num,bool enable)
{
    uint8_t command_send_buffer[16];

    param_struct_chorus.enable = enable;

    command_send_buffer[0]  = (uint8_t)(enable ? 1 : 0);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_CHORUS,EFFECT_PARAM_INDEX_CHORUS_ENABLE,command_send_buffer,1);
}
CommandProcess::Status CommandProcess::set_param_chorus_wet(int preset_num,double wet)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_chorus.wet = wet;

    params[0]               = (int16_t)(wet * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_CHORUS,EFFECT_PARAM_INDEX_CHORUS_WET,command_send_buffer,2);
}
CommandProcess::Status CommandProcess::set_param_chorus_dry(int preset_num,double dry)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_chorus.dry = dry;

    params[0]               = (int16_t)(dry * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_CHORUS,EFFECT_PARAM_INDEX_CHORUS_DRY,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_chorus_pitch(int preset_num,double pitch)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_chorus.pitch = pitch;

    params[0]               = (int16_t)(pitch * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_CHORUS,EFFECT_PARAM_INDEX_CHORUS_PITCH,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_chorus_rate(int preset_num,double rate)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_chorus.rate = rate;

    params[0]               = (int16_t)(rate * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_CHORUS,EFFECT_PARAM_INDEX_CHORUS_RATE,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_chorus_tone(int preset_num,double tone)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_chorus.tone = tone;

    params[0]               = (int16_t)(tone * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_CHORUS,EFFECT_PARAM_INDEX_CHORUS_TONE,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_delay(int preset_num,PanelDelay::param_struct &param)
{
    uint8_t command_send_buffer[16];
    int16_t params[10];

    param_struct_delay.enable = param.enable;
    param_struct_delay.feedback = param.feedback;
    param_struct_delay.time_ms = param.time_ms;
    param_struct_delay.tone = param.tone;
    param_struct_delay.wet = param.wet;

    params[0]               = (int16_t)param.enable;
    params[1]               = (int16_t)(param.feedback * PARAM_FLOAT_MUL);
    params[2]               = (int16_t)(param.wet * PARAM_FLOAT_MUL);
    params[3]               = (int16_t)(param.time_ms * PARAM_FLOAT_MUL);
    params[4]               = (int16_t)(param.tone * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)params[0];
    command_send_buffer[1]  = (uint8_t)(params[1]        & 0x00FF);
    command_send_buffer[2]  = (uint8_t)((params[1] >> 8) & 0x00FF);
    command_send_buffer[3]  = (uint8_t)(params[2]        & 0x00FF);
    command_send_buffer[4]  = (uint8_t)((params[2] >> 8) & 0x00FF);
    command_send_buffer[5]  = (uint8_t)(params[3]        & 0x00FF);
    command_send_buffer[6]  = (uint8_t)((params[3] >> 8) & 0x00FF);
    command_send_buffer[7]  = (uint8_t)(params[4]        & 0x00FF);
    command_send_buffer[8]  = (uint8_t)((params[4] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_DELAY,EFFECT_PARAM_INDEX_DELAY,command_send_buffer,9);
}

CommandProcess::Status CommandProcess::set_param_delay_enable(int preset_num,bool enable)
{
    uint8_t command_send_buffer[16];

    param_struct_delay.enable = enable;

    command_send_buffer[0]  = (uint8_t)(enable ? 1 : 0);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_DELAY,EFFECT_PARAM_INDEX_DELAY_ENABLE,command_send_buffer,1);
}
CommandProcess::Status CommandProcess::set_param_delay_time(int preset_num,double time_ms)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_delay.time_ms = time_ms;

    params[0]               = (int16_t)(time_ms * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_DELAY,EFFECT_PARAM_INDEX_DELAY_TIME,command_send_buffer,2);
}
CommandProcess::Status CommandProcess::set_param_delay_tone(int preset_num,double tone)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_delay.tone = tone;

    params[0]               = (int16_t)(tone * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_DELAY,EFFECT_PARAM_INDEX_DELAY_TONE,command_send_buffer,2);
}
CommandProcess::Status CommandProcess::set_param_delay_feedback(int preset_num,double feedback)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_delay.feedback = feedback;

    params[0]               = (int16_t)(feedback * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_DELAY,EFFECT_PARAM_INDEX_DELAY_FEEDBACK,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_delay_wet(int preset_num,double wet)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_delay.wet = wet;

    params[0]               = (int16_t)(wet * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_DELAY,EFFECT_PARAM_INDEX_DELAY_WET,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_wah(int preset_num,PanelWah::param_struct &param)
{
    uint8_t command_send_buffer[16];
    int16_t params[10];

    param_struct_wah.enable = param.enable;
    param_struct_wah.filter_gain = param.filter_gain;
    param_struct_wah.speed = param.speed;
    param_struct_wah.filter_q = param.filter_q;
    param_struct_wah.filter_freq_center = param.filter_freq_center;
    param_struct_wah.filter_freq_width = param.filter_freq_width;

    params[0]               = (int16_t)param.enable;
    params[1]               = (int16_t)(param.speed * PARAM_FLOAT_MUL);
    params[2]               = (int16_t)(param.filter_freq_center);
    params[3]               = (int16_t)(param.filter_freq_width);
    params[4]               = (int16_t)(param.filter_gain * PARAM_FLOAT_MUL);
    params[5]               = (int16_t)(param.filter_q * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)params[0];
    command_send_buffer[1]  = (uint8_t)(params[1]        & 0x00FF);
    command_send_buffer[2]  = (uint8_t)((params[1] >> 8) & 0x00FF);
    command_send_buffer[3]  = (uint8_t)(params[2]        & 0x00FF);
    command_send_buffer[4]  = (uint8_t)((params[2] >> 8) & 0x00FF);
    command_send_buffer[5]  = (uint8_t)(params[3]        & 0x00FF);
    command_send_buffer[6]  = (uint8_t)((params[3] >> 8) & 0x00FF);
    command_send_buffer[7]  = (uint8_t)(params[4]        & 0x00FF);
    command_send_buffer[8]  = (uint8_t)((params[4] >> 8) & 0x00FF);
    command_send_buffer[9]  = (uint8_t)(params[5]        & 0x00FF);
    command_send_buffer[10]  = (uint8_t)((params[5] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_WAH,EFFECT_PARAM_INDEX_WAH,command_send_buffer,11);
}

CommandProcess::Status CommandProcess::set_param_wah_enable(int preset_num,bool enable)
{
    uint8_t command_send_buffer[16];

    param_struct_wah.enable = enable;

    command_send_buffer[0]  = (uint8_t)(enable ? 1 : 0);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_WAH,EFFECT_PARAM_INDEX_WAH_ENABLE,command_send_buffer,1);
}
CommandProcess::Status CommandProcess::set_param_wah_speed(int preset_num,double speed)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_wah.speed = speed;

    params[0]               = (int16_t)(speed * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_WAH,EFFECT_PARAM_INDEX_WAH_SPEED,command_send_buffer,2);
}
CommandProcess::Status CommandProcess::set_param_wah_filter_q(int preset_num,double filter_q)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_wah.filter_q = filter_q;

    params[0]               = (int16_t)(filter_q * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_WAH,EFFECT_PARAM_INDEX_WAH_FILTER_Q,command_send_buffer,2);
}
CommandProcess::Status CommandProcess::set_param_wah_filter_gain(int preset_num,double filter_gain)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_wah.filter_gain = filter_gain;

    params[0]               = (int16_t)(filter_gain * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_WAH,EFFECT_PARAM_INDEX_WAH_FILTER_GAIN,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_wah_filter_freq_center(int preset_num,double filter_freq_center)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_wah.filter_freq_center = filter_freq_center;

    params[0]               = (uint16_t)(filter_freq_center);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_WAH,EFFECT_PARAM_INDEX_WAH_FILTER_FREQ_CENTER,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_wah_filter_freq_width(int preset_num,double filter_freq_width)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_wah.filter_freq_width = filter_freq_width;

    params[0]               = (int16_t)(filter_freq_width);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_WAH,EFFECT_PARAM_INDEX_WAH_FILTER_FREQ_WIDTH,command_send_buffer,2);
}


CommandProcess::Status CommandProcess::set_param_compressor(int preset_num,PanelCompressor::param_struct &param)
{
    uint8_t command_send_buffer[16];
    int16_t params[10];

    param_struct_cmp.enable = param.enable;
    param_struct_cmp.attack_time = param.attack_time;
    param_struct_cmp.threshold = param.threshold;
    param_struct_cmp.ratio = param.ratio;
    param_struct_cmp.release_time = param.release_time;
    param_struct_cmp.postgain = param.postgain;

    params[0]               = (int16_t)param.enable;
    params[1]               = (int16_t)(param.threshold * PARAM_FLOAT_MUL);
    params[2]               = (int16_t)(param.ratio * PARAM_FLOAT_MUL);
    params[3]               = (int16_t)(param.attack_time * PARAM_FLOAT_MUL);
    params[4]               = (int16_t)(param.release_time * PARAM_FLOAT_MUL);
    params[5]               = (int16_t)(param.postgain * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)params[0];
    command_send_buffer[1]  = (uint8_t)(params[1]        & 0x00FF);
    command_send_buffer[2]  = (uint8_t)((params[1] >> 8) & 0x00FF);
    command_send_buffer[3]  = (uint8_t)(params[2]        & 0x00FF);
    command_send_buffer[4]  = (uint8_t)((params[2] >> 8) & 0x00FF);
    command_send_buffer[5]  = (uint8_t)(params[3]        & 0x00FF);
    command_send_buffer[6]  = (uint8_t)((params[3] >> 8) & 0x00FF);
    command_send_buffer[7]  = (uint8_t)(params[4]        & 0x00FF);
    command_send_buffer[8]  = (uint8_t)((params[4] >> 8) & 0x00FF);
    command_send_buffer[9]  = (uint8_t)(params[5]        & 0x00FF);
    command_send_buffer[10]  = (uint8_t)((params[5] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_COMPRESSOR,EFFECT_PARAM_INDEX_COMPRESSOR,command_send_buffer,11);
}

CommandProcess::Status CommandProcess::set_param_compressor_enable(int preset_num,bool enable)
{
    uint8_t command_send_buffer[16];

    param_struct_cmp.enable = enable;

    command_send_buffer[0]  = (uint8_t)(enable ? 1 : 0);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_COMPRESSOR,EFFECT_PARAM_INDEX_COMPRESSOR_ENABLE,command_send_buffer,1);
}
CommandProcess::Status CommandProcess::set_param_compressor_threshold(int preset_num,double threshold)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_cmp.threshold = threshold;

    params[0]               = (int16_t)(threshold * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_COMPRESSOR,EFFECT_PARAM_INDEX_COMPRESSOR_THRESHOLD,command_send_buffer,2);
}
CommandProcess::Status CommandProcess::set_param_compressor_ratio(int preset_num,double ratio)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_cmp.ratio = ratio;

    params[0]               = (int16_t)(ratio * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_COMPRESSOR,EFFECT_PARAM_INDEX_COMPRESSOR_RATIO,command_send_buffer,2);
}
CommandProcess::Status CommandProcess::set_param_compressor_attack_time(int preset_num,double attack_time)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_cmp.attack_time = attack_time;

    params[0]               = (int16_t)(attack_time * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_COMPRESSOR,EFFECT_PARAM_INDEX_COMPRESSOR_ATTACK_TIME,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_compressor_release_time(int preset_num,double release_time)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_cmp.release_time = release_time;

    params[0]               = (uint16_t)(release_time * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_COMPRESSOR,EFFECT_PARAM_INDEX_COMPRESSOR_RELEASE_TIME,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_compressor_postgain(int preset_num,double postgain)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_cmp.postgain = postgain;

    params[0]               = (int16_t)(postgain * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_COMPRESSOR,EFFECT_PARAM_INDEX_COMPRESSOR_POSTGAIN,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_distortion(int preset_num,PanelDistortion::param_struct &param)
{
    uint8_t command_send_buffer[16];
    int16_t params[10];

    param_struct_distortion.enable = param.enable;
    param_struct_distortion.gain_input = param.gain_input;
    param_struct_distortion.gain_output = param.gain_output;
    param_struct_distortion.drive = param.drive;
    param_struct_distortion.tone = param.tone;
    param_struct_distortion.bandwidth = param.bandwidth;
    param_struct_distortion.freq = param.freq;

    params[0]               = (int16_t)param.enable;
    params[1]               = (int16_t)(param.gain_input * PARAM_FLOAT_MUL);
    params[2]               = (int16_t)(param.gain_output * PARAM_FLOAT_MUL);
    params[3]               = (int16_t)(param.drive * PARAM_FLOAT_MUL);
    params[4]               = (int16_t)(param.tone * PARAM_FLOAT_MUL);
    params[5]               = (int16_t)(param.bandwidth * PARAM_FLOAT_MUL);
    params[6]               = (int16_t)(param.freq);
    command_send_buffer[0]  = (uint8_t)params[0];
    command_send_buffer[1]  = (uint8_t)(params[1]        & 0x00FF);
    command_send_buffer[2]  = (uint8_t)((params[1] >> 8) & 0x00FF);
    command_send_buffer[3]  = (uint8_t)(params[2]        & 0x00FF);
    command_send_buffer[4]  = (uint8_t)((params[2] >> 8) & 0x00FF);
    command_send_buffer[5]  = (uint8_t)(params[3]        & 0x00FF);
    command_send_buffer[6]  = (uint8_t)((params[3] >> 8) & 0x00FF);
    command_send_buffer[7]  = (uint8_t)(params[4]        & 0x00FF);
    command_send_buffer[8]  = (uint8_t)((params[4] >> 8) & 0x00FF);
    command_send_buffer[9]  = (uint8_t)(params[5]        & 0x00FF);
    command_send_buffer[10]  = (uint8_t)((params[5] >> 8) & 0x00FF);
    command_send_buffer[11]  = (uint8_t)(params[6]        & 0x00FF);
    command_send_buffer[12]  = (uint8_t)((params[6] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_DISTORTION,EFFECT_PARAM_INDEX_DISTORTION,command_send_buffer,13);
}
CommandProcess::Status CommandProcess::set_param_distortion_enable(int preset_num,bool enable)
{
    uint8_t command_send_buffer[16];

    param_struct_distortion.enable = enable;

    command_send_buffer[0]  = (uint8_t)(enable ? 1 : 0);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_DISTORTION,EFFECT_PARAM_INDEX_DISTORTION_ENABLE,command_send_buffer,1);

}
CommandProcess::Status CommandProcess::set_param_distortion_gain_input(int preset_num,double gain)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_distortion.gain_input = gain;

    params[0]               = (int16_t)(gain * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_DISTORTION,EFFECT_PARAM_INDEX_DISTORTION_GAIN_INPUT,command_send_buffer,2);
}
CommandProcess::Status CommandProcess::set_param_distortion_gain_output(int preset_num,double gain)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_distortion.gain_output = gain;

    params[0]               = (int16_t)(gain * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_DISTORTION,EFFECT_PARAM_INDEX_DISTORTION_GAIN_OUTPUT,command_send_buffer,2);
}
CommandProcess::Status CommandProcess::set_param_distortion_drive(int preset_num,double drive)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_distortion.drive = drive;

    params[0]               = (int16_t)(drive * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_DISTORTION,EFFECT_PARAM_INDEX_DISTORTION_DRIVE,command_send_buffer,2);
}
CommandProcess::Status CommandProcess::set_param_distortion_tone(int preset_num,double tone)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_distortion.tone = tone;

    params[0]               = (int16_t)(tone * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_DISTORTION,EFFECT_PARAM_INDEX_DISTORTION_TONE,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_distortion_freq(int preset_num,double freq)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_distortion.freq = freq;

    params[0]               = (int16_t)(freq);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_DISTORTION,EFFECT_PARAM_INDEX_DISTORTION_FREQ,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_distortion_width(int preset_num,double bandwidth)
{
    uint8_t command_send_buffer[16];
    int16_t params[2];

    param_struct_distortion.bandwidth = bandwidth;

    params[0]               = (int16_t)(bandwidth * PARAM_FLOAT_MUL);
    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)((params[0] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_DISTORTION,EFFECT_PARAM_INDEX_DISTORTION_WIDGET,command_send_buffer,2);
}

CommandProcess::Status CommandProcess::set_param_distortion_eq(int preset_num,int band,FilterParam &eq_param)
{
    uint8_t command_send_buffer[16];
    int16_t params[5];

    params[0] = band;
    params[1] = eq_param.type;
    params[2] = eq_param.frequency;
    params[3] = eq_param.gain * PARAM_FLOAT_MUL;
    params[4] = eq_param.qfactor * PARAM_FLOAT_MUL;

    command_send_buffer[0]  = (uint8_t)(params[0]        & 0x00FF);
    command_send_buffer[1]  = (uint8_t)(params[1]        & 0x00FF);
    command_send_buffer[2]  = (uint8_t)(params[2]        & 0x00FF);
    command_send_buffer[3]  = (uint8_t)((params[2] >> 8) & 0x00FF);
    command_send_buffer[4]  = (uint8_t)(params[3]        & 0x00FF);
    command_send_buffer[5]  = (uint8_t)((params[3] >> 8) & 0x00FF);
    command_send_buffer[6]  = (uint8_t)(params[4]        & 0x00FF);
    command_send_buffer[7]  = (uint8_t)((params[4] >> 8) & 0x00FF);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_DISTORTION,EFFECT_PARAM_INDEX_DISTORTION_EQ,command_send_buffer,8);
}

CommandProcess::Status CommandProcess::set_param_preset_index(int preset_num)
{
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_PRESET,EFFECT_PARAM_INDEX_PRESET_CHANGE,0,0);
}

CommandProcess::Status CommandProcess::set_param_preset_group(int preset_num,uint32_t group_mark)
{
    uint8_t command_send_buffer[6];
    uint8_t *pu8 = (uint8_t *)&group_mark;

    command_send_buffer[0]  = (uint8_t)(pu8[0]);
    command_send_buffer[1]  = (uint8_t)(pu8[1]);
    command_send_buffer[2]  = (uint8_t)(pu8[2]);
    command_send_buffer[3]  = (uint8_t)(pu8[3]);
    return send_get_param_command_data(preset_num,CMD_INDEX_EFFECT_PRESET,EFFECT_PARAM_INDEX_PRESET_SET_GROUP,command_send_buffer,4);
}

