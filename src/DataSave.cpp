#include "DataSave.h"


DataSave::DataSave(wxString &path)
{
    file_path = path;
    wxString file_directory = file_path.BeforeLast('/');
    if(!wxDirExists(wxString(file_directory)))
    {
        if (!wxMkdir(file_directory))
        {
            wxMessageBox(wxString::Format("%s%s","创建路径失败：" , file_directory));
            return;
        }
    }
    // 读取XML文件
    if (!xmlDoc.Load(file_path))
    {
        wxLogInfo("Failed to load XML file.Create new XML file.");
        // 创建根节点
        wxXmlNode *rootNode = new wxXmlNode(wxXML_ELEMENT_NODE, "root");
        xmlDoc.SetDocumentNode(rootNode);

        wxXmlNode *childNode_config = new wxXmlNode(wxXML_ELEMENT_NODE, "Config");
        rootNode->AddChild(childNode_config);
    }
    // 遍历子节点
    wxXmlNode *node_config = FindChildNode(xmlDoc.GetRoot(),"Preset");
    wxXmlNode *node_effect = FindChildNode(node_config,"effect");
    save_node_reverb = FindChildNode(node_effect,"reverb");
    save_node_chorus = FindChildNode(node_effect,"chorus");
    save_node_delay  = FindChildNode(node_effect,"delay");
    save_node_wah  = FindChildNode(node_effect,"wah");
    save_node_distortion  = FindChildNode(node_effect,"distortion");
    //save_node_distortion2  = FindChildNode(node_effect,"distortion2");
}


DataSave::~DataSave()
{

}

wxXmlNode *DataSave::FindChildNode(wxXmlNode *node, const char *name,bool autoCreate)
{
    if (node)
    {
        while(1)
        {
            wxXmlNode *child = node->GetChildren();
            while (child)
            {
                if(strcmp(child->GetName().c_str(), name) == 0)
                {
                    return child;
                }
                child = child->GetNext();
            }
            if(autoCreate)
            {
                node->AddChild(new wxXmlNode(wxXML_ELEMENT_NODE, name));
            }
        }
    }
    return NULL;
}

bool DataSave::NodeAttributesSetValue(wxXmlNode *node, const char *attr_name, const char *value,bool autoCreate)
{
    if(node)
    {
        wxXmlAttribute *attr = node->GetAttributes();
        while (attr)
        {
            if(strcmp(attr->GetName().c_str(), attr_name) == 0)
            {
                attr->SetValue(value);
                return true;
            }
            attr = attr->GetNext();
        }

        if(autoCreate)
        {
            node->AddAttribute(new wxXmlAttribute(attr_name, value));
            return true;
        }
    }

    return false;
}

bool DataSave::NodeAttributesGetValue(wxXmlNode *node, const char *attr_name, wxString &value)
{
    value = "";
    if(node)
    {
        wxXmlAttribute *attr = node->GetAttributes();
        while (attr)
        {
            if(strcmp(attr->GetName().c_str(), attr_name) == 0)
            {
                value = attr->GetValue();
                return true;
            }
            attr = attr->GetNext();
        }
    }

    return false;
}

wxXmlNode *DataSave::GetEffectParamNode(node_type type)
{
    switch(type)
    {
    case NODE_REVERB:
        return save_node_reverb;
    case NODE_CHORUS:
        return save_node_chorus;
    case NODE_DELAY:
        return save_node_delay;
    case NODE_DISTORTION:
        return save_node_distortion;
    case NODE_WAH:
        return save_node_wah;
    case NODE_COMPRESS:
        // return save_node_compress;
    default:
        return NULL;
    }
}

bool DataSave::Save(void)
{
    if (!xmlDoc.Save(file_path))
    {
        wxLogError("Failed to save XML file.");
        return false;
    }

    return true;
}