#ifndef __PANEL_EQUALIZER_H__
#define __PANEL_EQUALIZER_H__

#include <wx/wx.h>
#include <wx/aui/aui.h>
#include <wx/wrapsizer.h>
#include "ctrl_equalizer.h"
#include <wx/xml/xml.h>




class PanelEqualizer : public wxPanel
{
public:
    PanelEqualizer(const wxString &title,int sample_rate = 48000, wxWindowID id = wxID_ANY, wxWindow *parent = nullptr);

private:
    wxAuiPaneInfo paneInfo;
    wxBoxSizer *layout;
    GraphicEqualizer *equalizer;

public:
    wxAuiPaneInfo &GetPaneInfo();
    void SetShow(bool show);
    void OnEqualizerEvent(MyEventEqualizer &event);
    void OnEqualizerFrequencyEvent(MyEventEqualizer &event);
    void OnEqualizerGainEvent(MyEventEqualizer &event);
    void OnEqualizerQFactorEvent(MyEventEqualizer &event);
    void OnEqualizerTypeEvent(MyEventEqualizer &event);

    void AddFilter(FilterType type, double frequency, double gain,double qfactor);

    wxDECLARE_EVENT_TABLE();
};


#endif
