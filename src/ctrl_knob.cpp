#include "ctrl_knob.h"
#include <wx/dcgraph.h>

wxBEGIN_EVENT_TABLE(MyKnob, wxPanel)
    EVT_PAINT(MyKnob::OnPaint)
    EVT_SIZE  (MyKnob::OnSize)
    EVT_LEFT_DOWN(MyKnob::OnMouseDown)
    EVT_LEFT_UP(MyKnob::OnMouseUp)
    EVT_MOTION(MyKnob::OnMouseDrag)
wxEND_EVENT_TABLE()

MyKnob::MyKnob(wxWindow* parent, wxWindowID id)
    : wxPanel(parent, id)
{
    // 初始化参数
    m_value = 0;
}

void MyKnob::OnSize(wxSizeEvent &event)
{
    Refresh();
    event.Skip();
}

void MyKnob::OnPaint(wxPaintEvent& event)
{
    wxPaintDC dc(this);
    wxMemoryDC memDC;
    wxBitmap bitmap(GetClientSize());
    memDC.SelectObject(bitmap);
    memDC.SetBackground(wxBrush(wxColour(140, 140, 140)));
    memDC.Clear();

    wxGraphicsContext *gc = wxGraphicsContext::Create(memDC);
    if(gc)
    {
        gc->SetAntialiasMode(wxANTIALIAS_DEFAULT);
    }
    wxGCDC gcdc;
    gcdc.SetBackground(GetBackgroundColour());
    gcdc.SetGraphicsContext(gc);
    wxDC &wxdc = static_cast<wxDC&>(gcdc);
    wxRect rect(GetClientSize().GetX() * 0.2,GetClientSize().GetY() * 0.2,GetClientSize().GetWidth() * 0.6,GetClientSize().GetHeight() * 0.5);
    wxdc.DrawRoundedRectangle(rect, 5);

    dc.Blit(0, 0, GetClientSize().GetWidth(), GetClientSize().GetHeight(), &memDC, 0, 0);
}

void MyKnob::OnMouseDown(wxMouseEvent& event)
{
    // 处理鼠标按下事件，记录初始点击位置
}

void MyKnob::OnMouseDrag(wxMouseEvent& event)
{
    // 处理鼠标拖动事件，根据拖动距离调节参数值
}

void MyKnob::OnMouseUp(wxMouseEvent& event)
{
    // 处理鼠标释放事件
}

