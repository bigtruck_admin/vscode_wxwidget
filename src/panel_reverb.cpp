#include <wx/wx.h>
#include "panel_reverb.h"
#include "MyCommon.h"
#include "DataSave.h"

#define SLI_MAX(max,min,step) (int)((max - min) / step)


wxDEFINE_EVENT(myEVT_REVERB, MyEventReverb);
wxDEFINE_EVENT(myEVT_REVERB_DRY, MyEventReverb);
wxDEFINE_EVENT(myEVT_REVERB_WET, MyEventReverb);
wxDEFINE_EVENT(myEVT_REVERB_DAMPING, MyEventReverb);
wxDEFINE_EVENT(myEVT_REVERB_FEEDBACK, MyEventReverb);
wxDEFINE_EVENT(myEVT_REVERB_TONE, MyEventReverb);
wxDEFINE_EVENT(myEVT_REVERB_ENABLE, MyEventReverb);


// wxBEGIN_EVENT_TABLE(PanelReverb, wxPanel)
//     EVT_SLIDER(PanelReverb::ID_SLI_DRY, PanelReverb::OnSliderChange)
//     EVT_SLIDER(PanelReverb::ID_SLI_WET, PanelReverb::OnSliderChange)
//     EVT_SLIDER(PanelReverb::ID_SLI_DAMPING, PanelReverb::OnSliderChange)
//     EVT_SLIDER(PanelReverb::ID_SLI_FEEDBACK, PanelReverb::OnSliderChange)
//     EVT_SLIDER(PanelReverb::ID_SLI_TONE, PanelReverb::OnSliderChange)
//     EVT_TEXT_ENTER(PanelReverb::ID_TXT_DRY,PanelReverb::OnTextEnter)
//     EVT_TEXT_ENTER(PanelReverb::ID_TXT_WET,PanelReverb::OnTextEnter)
//     EVT_TEXT_ENTER(PanelReverb::ID_TXT_DAMPING,PanelReverb::OnTextEnter)
//     EVT_TEXT_ENTER(PanelReverb::ID_TXT_FEEDBACK,PanelReverb::OnTextEnter)
//     EVT_TEXT_ENTER(PanelReverb::ID_TXT_TONE,PanelReverb::OnTextEnter)
//     EVT_CHECKBOX(PanelReverb::ID_CHK_SWITCH,PanelReverb::OnChkChange)
// wxEND_EVENT_TABLE()


PanelReverb::PanelReverb(const wxString &title, wxXmlNode *node, wxWindowID id, wxWindow *parent)
    : wxPanel(parent,id,wxDefaultPosition,wxDefaultSize,wxTAB_TRAVERSAL)
{
    SetSize(200, 300);

    root_node = node;
    select_index = 0;

    wxWindowID  cid = id + 1;
    ID_TXT_DRY = cid++;
    ID_TXT_WET = cid++;
    ID_TXT_FEEDBACK = cid++;
    ID_TXT_TONE = cid++;
    ID_TXT_DAMPING = cid++;
    ID_SLI_DRY = cid++;
    ID_SLI_WET = cid++;
    ID_SLI_FEEDBACK = cid++;
    ID_SLI_TONE = cid++;
    ID_SLI_DAMPING = cid++;
    ID_CHK_SWITCH = cid++;
    ID_MENU_VIEW_REVERB = cid++;
    ID_CHO_PRESET = cid ++;

    wxCheckBox *chk_enable       = new wxCheckBox(this  , ID_CHK_SWITCH  , "Enable");
    wxTextCtrl *txt_dry          = new wxTextCtrl(this  , ID_TXT_DRY     , mDoubleToString(dry_min),wxDefaultPosition,wxDefaultSize,wxTE_PROCESS_ENTER);
    wxTextCtrl *txt_wet          = new wxTextCtrl(this  , ID_TXT_WET     , mDoubleToString(wet_min),wxDefaultPosition,wxDefaultSize,wxTE_PROCESS_ENTER);
    wxTextCtrl *txt_damp      = new wxTextCtrl(this  , ID_TXT_DAMPING , mDoubleToString(damp_min),wxDefaultPosition,wxDefaultSize,wxTE_PROCESS_ENTER);
    wxTextCtrl *txt_feedback     = new wxTextCtrl(this  , ID_TXT_FEEDBACK, mDoubleToString(feedback_min),wxDefaultPosition,wxDefaultSize,wxTE_PROCESS_ENTER);
    wxTextCtrl *txt_tone         = new wxTextCtrl(this  , ID_TXT_TONE    , mDoubleToString(tone_min),wxDefaultPosition,wxDefaultSize,wxTE_PROCESS_ENTER);
    wxStaticText *lab_dry        = new wxStaticText(this, wxID_ANY       , "Dry:");
    wxStaticText *lab_wet        = new wxStaticText(this, wxID_ANY       , "Wet:");
    wxStaticText *lab_damp    = new wxStaticText(this, wxID_ANY       , "Damping:");
    wxStaticText *lab_feedback   = new wxStaticText(this, wxID_ANY       , "Feedback:");
    wxStaticText *lab_tone       = new wxStaticText(this, wxID_ANY       , "Tone:");
    wxStaticText *lab_preset      = new wxStaticText(this, wxID_ANY       , "Preset:");
    wxSlider *sli_dry            = new wxSlider(this    , ID_SLI_DRY     , 0            , 0  , SLI_MAX(dry_max,dry_min,dry_step));
    wxSlider *sli_wet            = new wxSlider(this    , ID_SLI_WET     , 0            , 0  , SLI_MAX(wet_max,wet_min,wet_step));
    wxSlider *sli_damp        = new wxSlider(this    , ID_SLI_DAMPING , 0            , 0  , SLI_MAX(damp_max,damp_min,damp_step));
    wxSlider *sli_feedback       = new wxSlider(this    , ID_SLI_FEEDBACK, 0            , 0  , SLI_MAX(feedback_max,feedback_min,feedback_step));
    wxSlider *sli_tone           = new wxSlider(this    , ID_SLI_TONE    , 0            , 0  , SLI_MAX(tone_max,tone_min,tone_step));
    wxFlexGridSizer *layout_main = new wxFlexGridSizer(7,3               ,wxSize(3      ,3));
    wxChoice        *cho_preset      = new wxChoice(this,     ID_CHO_PRESET);

    sli_dry->SetMinSize(wxSize(100, -1));
    sli_dry->SetTick(1);
    sli_wet->SetMinSize(wxSize(100, -1));
    sli_damp->SetMinSize(wxSize(100, -1));
    sli_feedback->SetMinSize(wxSize(100, -1));
    sli_tone->SetMinSize(wxSize(100, -1));
    txt_dry->SetMinSize(wxSize(50, -1));
    txt_dry->SetMaxSize(wxSize(100, -1));
    txt_wet->SetMinSize(wxSize(50, -1));
    txt_wet->SetMaxSize(wxSize(100, -1));
    txt_damp->SetMinSize(wxSize(50, -1));
    txt_damp->SetMaxSize(wxSize(100, -1));
    txt_feedback->SetMinSize(wxSize(50, -1));
    txt_feedback->SetMaxSize(wxSize(100, -1));
    txt_tone->SetMinSize(wxSize(50, -1));
    txt_tone->SetMaxSize(wxSize(100, -1));

    sli_damp->Bind(wxEVT_SLIDER, &PanelReverb::OnSliderChange, this);
    sli_dry->Bind(wxEVT_SLIDER, &PanelReverb::OnSliderChange, this);
    sli_feedback->Bind(wxEVT_SLIDER, &PanelReverb::OnSliderChange, this);
    sli_tone->Bind(wxEVT_SLIDER, &PanelReverb::OnSliderChange, this);
    sli_wet->Bind(wxEVT_SLIDER, &PanelReverb::OnSliderChange, this);
    txt_damp->Bind(wxEVT_TEXT_ENTER, &PanelReverb::OnTextEnter, this);
    txt_dry->Bind(wxEVT_TEXT_ENTER, &PanelReverb::OnTextEnter, this);
    txt_feedback->Bind(wxEVT_TEXT_ENTER, &PanelReverb::OnTextEnter, this);
    txt_tone->Bind(wxEVT_TEXT_ENTER, &PanelReverb::OnTextEnter, this);
    txt_wet->Bind(wxEVT_TEXT_ENTER, &PanelReverb::OnTextEnter, this);
    chk_enable->Bind(wxEVT_CHECKBOX, &PanelReverb::OnChkChange, this);
    cho_preset->Bind(wxEVT_CHOICE,        &PanelReverb::OnCombChange,   this, ID_CHO_PRESET);

    // cho_preset->Append("reverb-1");
    // cho_preset->Append("reverb-2");
    // cho_preset->Append("reverb-3");
    // cho_preset->Append("reverb-4");
    // cho_preset->Append("reverb-5");
    // cho_preset->Append("reverb-6");
    // cho_preset->Select(select_index);

    layout_main->Add(chk_enable  ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->AddStretchSpacer();
    layout_main->AddStretchSpacer();
    layout_main->Add(lab_dry     ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->Add(txt_dry     ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->Add(sli_dry     ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->Add(lab_wet     ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->Add(txt_wet     ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->Add(sli_wet     ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->Add(lab_damp ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->Add(txt_damp ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->Add(sli_damp ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->Add(lab_feedback,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->Add(txt_feedback,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->Add(sli_feedback,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->Add(lab_tone    ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->Add(txt_tone    ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->Add(sli_tone    ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->Add(lab_preset,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->Add(cho_preset,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
    layout_main->AddStretchSpacer();
    layout_main->AddGrowableCol(0,0);
    layout_main->AddGrowableCol(1,0);
    layout_main->AddGrowableCol(2,1);

    SetSizer(layout_main);

    paneInfo.Caption(title);
    paneInfo.Dock();
    paneInfo.Dockable(true);
    paneInfo.CloseButton(true);
    paneInfo.MaximizeButton(true);
    paneInfo.MinimizeButton(true);
    paneInfo.DestroyOnClose(false);
    // paneInfo.Direction(wxLeft);
    param.dry = dry_min;
    param.wet = wet_min;
    param.damp = damp_min;
    param.feedback = feedback_min;
    param.tone = tone_min;
    param.enable = false;

    param_event.SetEventObject(this);

    // wxString str;
    // wxString select_name = attr_name_preset_default;
    // if(DataSave::NodeAttributesGetValue(root_node,attr_name_select,str))
    // {
    //     select_name = str;
    // }
    // else
    // {
    //     DataSave::NodeAttributesSetValue(root_node,attr_name_select,attr_name_preset_default);
    // }
    preset_node = nullptr;
    if(cho_preset->GetCount() > 0)
    {
        preset_node = SelectPreset(cho_preset->GetString(select_index));
    }
}


wxAuiPaneInfo &PanelReverb::GetPaneInfo()
{
    return paneInfo;
}


void PanelReverb::SetShow(bool show)
{
    Show(show);
}

void PanelReverb::AppendToMenu(wxMenu *menu,bool is_checkable)
{
    menu->AppendCheckItem(ID_MENU_VIEW_REVERB, paneInfo.caption,wxString::Format("Show %s panel",paneInfo.caption.c_str()));
    menu->Check(ID_MENU_VIEW_REVERB, is_checkable);
}


void PanelReverb::OnSliderChange(wxCommandEvent& event)
{
    int id = event.GetId();
    // switch(id)
    {
        if(id == ID_SLI_DRY)
        {
            wxSlider *sli = wxDynamicCast(FindWindowById(ID_SLI_DRY),wxSlider);
            wxTextCtrl *txt = wxDynamicCast(FindWindowById(ID_TXT_DRY),wxTextCtrl);
            double sv = sli->GetValue();
            double v = dry_min + sv / (1/dry_step);
            txt->SetValue(mDoubleToString(v));
            param.dry = v;
            TrigerEvent(EV_DRY);
        }
        else if(id == ID_SLI_WET)
        {
            wxSlider *sli = wxDynamicCast(FindWindowById(ID_SLI_WET),wxSlider);
            wxTextCtrl *txt = wxDynamicCast(FindWindowById(ID_TXT_WET),wxTextCtrl);
            double sv = sli->GetValue();
            double v = wet_min + sv / (1/wet_step);
            txt->SetValue(mDoubleToString(v));
            param.wet = v;
            TrigerEvent(EV_WET);
        }
        else if(id == ID_SLI_DAMPING)
        {
            wxSlider *sli = wxDynamicCast(FindWindowById(ID_SLI_DAMPING),wxSlider);
            wxTextCtrl *txt = wxDynamicCast(FindWindowById(ID_TXT_DAMPING),wxTextCtrl);
            double sv = sli->GetValue();
            double v = damp_min + sv / (1/damp_step);
            txt->SetValue(mDoubleToString(v));
            param.damp = v;
            TrigerEvent(EV_DAMPING);
        }
        else if(id == ID_SLI_TONE)
        {
            wxSlider *sli = wxDynamicCast(FindWindowById(ID_SLI_TONE),wxSlider);
            wxTextCtrl *txt = wxDynamicCast(FindWindowById(ID_TXT_TONE),wxTextCtrl);
            double sv = sli->GetValue();
            double v = tone_min + sv / (1/tone_step);
            txt->SetValue(mDoubleToString(v));
            param.tone = v;
            TrigerEvent(EV_TONE);
        }
        else if(id == ID_SLI_FEEDBACK)
        {
            wxSlider *sli = wxDynamicCast(FindWindowById(ID_SLI_FEEDBACK),wxSlider);
            wxTextCtrl *txt = wxDynamicCast(FindWindowById(ID_TXT_FEEDBACK),wxTextCtrl);
            double sv = sli->GetValue();
            double v = feedback_min + sv / (1/feedback_step);
            txt->SetValue(mDoubleToString(v));
            param.feedback = v;
            TrigerEvent(EV_FEEDBACK);
        }
    }
}

void PanelReverb::OnTextEnter(wxCommandEvent& event)
{
    int type = event.GetEventType();
    int id = event.GetId();
    if(wxEVT_TEXT_ENTER == type)
    {
        if(id == ID_TXT_DRY)
        {
            wxTextCtrl *txt = wxDynamicCast(FindWindowById(ID_TXT_DRY),wxTextCtrl);
            double v ;
            if(txt->GetValue().ToDouble(&v) == true)
            {
                if((v >= dry_min) && (v <= dry_max))
                {
                    param.dry = v;
                }
            }
            double sv = (param.dry - dry_min) * (1/dry_step);
            wxSlider *sli = wxDynamicCast(FindWindowById(ID_SLI_DRY),wxSlider);
            sli->SetValue(sv);
            TrigerEvent(EV_DRY);
        }
        else if(id == ID_TXT_WET)
        {
            wxTextCtrl *txt = wxDynamicCast(FindWindowById(ID_TXT_WET),wxTextCtrl);
            double v ;
            if(txt->GetValue().ToDouble(&v) == true)
            {
                if((v >= wet_min) && (v <= wet_max))
                {
                    param.wet = v;
                }
            }
            double sv = (param.wet - wet_min) * (1/wet_step);
            wxSlider *sli = wxDynamicCast(FindWindowById(ID_SLI_WET),wxSlider);
            sli->SetValue(sv);
            TrigerEvent(EV_WET);
        }
        else if(id == ID_TXT_DAMPING)
        {
            wxTextCtrl *txt = wxDynamicCast(FindWindowById(ID_TXT_DAMPING),wxTextCtrl);
            double v ;
            if(txt->GetValue().ToDouble(&v) == true)
            {
                if((v >= damp_min) && (v <= damp_max))
                {
                    param.damp = v;
                }
            }
            double sv = (param.damp - damp_min) * (1/damp_step);
            wxSlider *sli = wxDynamicCast(FindWindowById(ID_SLI_DAMPING),wxSlider);
            sli->SetValue(sv);
            TrigerEvent(EV_DAMPING);
        }
        else if(id == ID_TXT_TONE)
        {
            wxTextCtrl *txt = wxDynamicCast(FindWindowById(ID_TXT_TONE),wxTextCtrl);
            double v ;
            if(txt->GetValue().ToDouble(&v) == true)
            {
                if((v >= tone_min) && (v <= tone_max))
                {
                    param.tone = v;
                }
            }
            double sv = (param.tone - tone_min) * (1/tone_step);
            wxSlider *sli = wxDynamicCast(FindWindowById(ID_SLI_TONE),wxSlider);
            sli->SetValue(sv);
            TrigerEvent(EV_TONE);
        }
        else if(id == ID_TXT_FEEDBACK)
        {
            wxTextCtrl *txt = wxDynamicCast(FindWindowById(ID_TXT_FEEDBACK),wxTextCtrl);
            double v ;
            if(txt->GetValue().ToDouble(&v) == true)
            {
                if((v >= feedback_min) && (v <= feedback_max))
                {
                    param.feedback = v;
                }
            }
            double sv = (param.feedback - feedback_min) * (1/feedback_step);
            wxSlider *sli = wxDynamicCast(FindWindowById(ID_SLI_FEEDBACK),wxSlider);
            sli->SetValue(sv);
            TrigerEvent(EV_FEEDBACK);
        }
    }
}


void PanelReverb::OnChkChange(wxCommandEvent &event)
{
    int id = event.GetId();
    // switch(id)
    {
        if(id == ID_CHK_SWITCH)
        {
            wxCheckBox *chk = wxDynamicCast(FindWindowById(ID_CHK_SWITCH),wxCheckBox);
            param.enable = chk->IsChecked();
            TrigerEvent(EV_ENABLE);
        }
    }
}

void PanelReverb::OnCombChange(wxCommandEvent& event)
{
    wxChoice *cmb = wxDynamicCast(FindWindowById(ID_CHO_PRESET),wxChoice);
    int i = cmb->GetSelection();
    if((unsigned int)i < cmb->GetCount())
    {
        select_index = i;
        preset_node = SelectPreset(cmb->GetString(i));
        param_event.SetPresetIndex(select_index);
        param_event.SetEventType(myEVT_REVERB);
        ProcessWindowEvent(param_event);
    }
}


double PanelReverb::SetParamDry(double v)
{
    if(v < dry_min)
    {
        v = dry_min;
    }
    else if(v > dry_max)
    {
        v = dry_max;
    }
    double sv = (v - dry_min) * (1/dry_step);
    wxSlider *sli = wxDynamicCast(FindWindowById(ID_SLI_DRY),wxSlider);
    sli->SetValue(sv);
    wxTextCtrl *txt = wxDynamicCast(FindWindowById(ID_TXT_DRY),wxTextCtrl);
    txt->SetValue(mDoubleToString(v));
    param.dry = v;
    param_event.SetDry(v);
    return v;
}

double PanelReverb::SetParamWet(double v)
{
    if(v < wet_min)
    {
        v = wet_min;
    }
    else if(v > wet_max)
    {
        v = wet_max;
    }
    double sv = (v - wet_min) * (1/wet_step);
    wxSlider *sli = wxDynamicCast(FindWindowById(ID_SLI_WET),wxSlider);
    sli->SetValue(sv);
    wxTextCtrl *txt = wxDynamicCast(FindWindowById(ID_TXT_WET),wxTextCtrl);
    txt->SetValue(mDoubleToString(v));
    param.wet = v;
    param_event.SetWet(v);
    return v;
}

double PanelReverb::SetParamDamping(double v)
{
    if(v < damp_min)
    {
        v = damp_min;
    }
    else if(v > damp_max)
    {
        v = damp_max;
    }
    double sv = (v - damp_min) * (1/damp_step);
    wxSlider *sli = wxDynamicCast(FindWindowById(ID_SLI_DAMPING),wxSlider);
    sli->SetValue(sv);
    wxTextCtrl *txt = wxDynamicCast(FindWindowById(ID_TXT_DAMPING),wxTextCtrl);
    txt->SetValue(mDoubleToString(v));
    param.damp = v;
    param_event.SetDamping(v);
    return v;
}

double PanelReverb::SetParamTone(double v)
{
    if(v < tone_min)
    {
        v = tone_min;
    }
    else if(v > tone_max)
    {
        v = tone_max;
    }
    double sv = (v - tone_min) * (1/tone_step);
    wxSlider *sli = wxDynamicCast(FindWindowById(ID_SLI_TONE),wxSlider);
    sli->SetValue(sv);
    wxTextCtrl *txt = wxDynamicCast(FindWindowById(ID_TXT_TONE),wxTextCtrl);
    txt->SetValue(mDoubleToString(v));
    param.tone = v;
    param_event.SetTone(v);
    return v;
}

double PanelReverb::SetParamFeedback(double v)
{
    if(v < feedback_min)
    {
        v = feedback_min;
    }
    else if(v > feedback_max)
    {
        v = feedback_max;
    }
    double sv = (v - feedback_min) * (1/feedback_step);
    wxSlider *sli = wxDynamicCast(FindWindowById(ID_SLI_FEEDBACK),wxSlider);
    sli->SetValue(sv);
    wxTextCtrl *txt = wxDynamicCast(FindWindowById(ID_TXT_FEEDBACK),wxTextCtrl);
    txt->SetValue(mDoubleToString(v));
    param.feedback = v;
    param_event.SetFeedback(v);
    return v;
}

bool PanelReverb::SetParamEnable(bool v)
{
    wxCheckBox *chk = wxDynamicCast(FindWindowById(ID_CHK_SWITCH),wxCheckBox);
    chk->SetValue(v);
    param.enable = v;
    param_event.SetEnable(v);
    return v;
}

double PanelReverb::GetParamDry() { return param.dry; }
double PanelReverb::GetParamWet() { return param.wet; }
double PanelReverb::GetParamFeedback() { return param.feedback; }
double PanelReverb::GetParamTone() { return param.tone; }
double PanelReverb::GetParamDamping() { return param.damp; }
bool PanelReverb::GetParamEnable() { return param.enable; }

void PanelReverb::SetPresetIndex(const unsigned int index)
{
    wxChoice *cmb = wxDynamicCast(FindWindowById(ID_CHO_PRESET),wxChoice);
    if(index < cmb->GetCount())
    {
        select_index = index;
        cmb->SetSelection(index);
        param_event.SetPresetIndex(select_index);
        preset_node = SelectPreset(cmb->GetString(index));
    }
}

int PanelReverb::GetPresetIndex()
{
    return select_index;
}


void PanelReverb::TrigerEvent(int ev)
{
    switch(ev)
    {
        case EV_DRY:
        {
            param_event.SetEventType(myEVT_REVERB_DRY);
            param_event.SetDry(param.dry);
            ProcessWindowEvent(param_event);
            DataSave::NodeAttributesSetValue(preset_node, attr_name_param_dry, mDoubleToString(param.dry).c_str());
        }
        break;
        case EV_WET:
        {
            param_event.SetEventType(myEVT_REVERB_WET);
            param_event.SetWet(param.wet);
            ProcessWindowEvent(param_event);
            DataSave::NodeAttributesSetValue(preset_node, attr_name_param_wet, mDoubleToString(param.wet).c_str());
        }
        break;
        case EV_DAMPING:
        {
            param_event.SetEventType(myEVT_REVERB_DAMPING);
            param_event.SetDamping(param.damp);
            ProcessWindowEvent(param_event);
            DataSave::NodeAttributesSetValue(preset_node, attr_name_param_damp, mDoubleToString(param.damp).c_str());
        }
        break;
        case EV_TONE:
        {
            param_event.SetEventType(myEVT_REVERB_TONE);
            param_event.SetTone(param.tone);
            ProcessWindowEvent(param_event);
            DataSave::NodeAttributesSetValue(preset_node, attr_name_param_tone, mDoubleToString(param.tone).c_str());
        }
        break;
        case EV_FEEDBACK:
        {
            param_event.SetEventType(myEVT_REVERB_FEEDBACK);
            param_event.SetFeedback(param.feedback);
            ProcessWindowEvent(param_event);
            DataSave::NodeAttributesSetValue(preset_node, attr_name_param_feedback, mDoubleToString(param.feedback).c_str());
        }
        break;
        case EV_ENABLE:
        {
            param_event.SetEventType(myEVT_REVERB_ENABLE);
            param_event.SetEnable(param.enable);
            ProcessWindowEvent(param_event);
            DataSave::NodeAttributesSetValue(preset_node, attr_name_param_enable, param.enable ? "1" : "0");
        }
        break;
    }
}

wxXmlNode *PanelReverb::SelectPreset(wxString name)
{
    double dry = param.dry;
    double wet = param.wet;
    double damp = param.damp;
    double feedback = param.feedback;
    double tone = param.tone;
    bool enable = param.enable;
    wxString str;

    wxXmlNode *child;
    child = root_node->GetChildren();
    while(child)
    {
        if(strcmp(child->GetName().c_str(), name.c_str()) == 0)
        {
            break;
        }
        child = child->GetNext();
    }
    if(DataSave::NodeAttributesGetValue(child, attr_name_param_enable, str))
    {
        int v ;
        if(str.ToInt(&v))
        {
            enable = v > 0 ? true : false;
        }
    }
    if(DataSave::NodeAttributesGetValue(child,attr_name_param_dry,str))
    {
        double v;
        if(str.ToDouble(&v))
        {
            dry = v;
        }
    }
    if(DataSave::NodeAttributesGetValue(child,attr_name_param_wet,str))
    {
        double v;
        if(str.ToDouble(&v))
        {
            wet = v;
        }
    }
    if(DataSave::NodeAttributesGetValue(child, attr_name_param_damp, str))
    {
        double v;
        if(str.ToDouble(&v))
        {
            damp = v;
        }
    }
    if(DataSave::NodeAttributesGetValue(child, attr_name_param_feedback, str))
    {
        double v;
        if(str.ToDouble(&v))
        {
            feedback = v;
        }
    }
    if(DataSave::NodeAttributesGetValue(child, attr_name_param_tone, str))
    {
        double v;
        if(str.ToDouble(&v))
        {
            tone = v;
        }
    }

    SetParamEnable(enable);
    SetParamDry(dry);
    SetParamWet(wet);
    SetParamDamping(damp);
    SetParamFeedback(feedback);
    SetParamTone(tone);

    // if(child == NULL)
    // {
    //     child = new wxXmlNode(wxXML_ELEMENT_NODE,name);
    //     WriteToXML(child);
    //     root_node->AddChild(child);
    // }
    return child;
}

void PanelReverb::PresetListClear(void)
{
    wxChoice *cmb = wxDynamicCast(FindWindowById(ID_CHO_PRESET),wxChoice);
    if(cmb)
    {
        cmb->Clear();
        preset_node = nullptr;
    }
}

void PanelReverb::AddPresetList(const wxString &preset_name)
{
    wxChoice *cmb = wxDynamicCast(FindWindowById(ID_CHO_PRESET),wxChoice);
    if(cmb)
    {
        cmb->Append(preset_name);
    }
}

wxXmlNode *PanelReverb::SaveCurrentParamsToNewPreset(wxXmlNode *root, const wxString &new_preset_name,int &is_same)
{
    wxXmlNode *child = nullptr;
    is_same = 0;
    if((root != nullptr) && (new_preset_name.empty() == false))
    {
        //查找有没有名字相同的预设
        child = root->GetChildren();
        while(child)
        {
            if(strcmp(child->GetName().c_str(), new_preset_name.c_str()) == 0)
            {
                is_same = 1;
                return nullptr;
            }
            child = child->GetNext();
        }
        child = new wxXmlNode(wxXML_ELEMENT_NODE,new_preset_name);
        WriteToXML(child);
        root->AddChild(child);
        wxChoice *cmb = wxDynamicCast(FindWindowById(ID_CHO_PRESET),wxChoice);
        if(cmb)
        {
            cmb->Append(new_preset_name);
        }
    }
    return child;
}

bool PanelReverb::WriteToXML(wxXmlNode *node)
{
    if(node)
    {
        DataSave::NodeAttributesSetValue(node, attr_name_param_enable, param.enable ? "1" : "0");
        DataSave::NodeAttributesSetValue(node, attr_name_param_dry, mDoubleToString(param.dry).c_str());
        DataSave::NodeAttributesSetValue(node, attr_name_param_wet, mDoubleToString(param.wet).c_str());
        DataSave::NodeAttributesSetValue(node, attr_name_param_damp, mDoubleToString(param.damp).c_str());
        DataSave::NodeAttributesSetValue(node, attr_name_param_tone, mDoubleToString(param.tone).c_str());
        DataSave::NodeAttributesSetValue(node, attr_name_param_feedback, mDoubleToString(param.feedback).c_str());
        return true;
    }
    return false;
}
