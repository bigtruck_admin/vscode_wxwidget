#include <wx/wx.h>
#include <wx/event.h>
#include <wx/control.h>

class MyKnob : public wxPanel
{
public:
    MyKnob(wxWindow* parent, wxWindowID id);

    void OnSize(wxSizeEvent &event);
    void OnPaint(wxPaintEvent& event);
    void OnMouseDown(wxMouseEvent& event);
    void OnMouseDrag(wxMouseEvent& event);
    void OnMouseUp(wxMouseEvent& event);

private:
    int m_value;

    wxDECLARE_EVENT_TABLE();
};
