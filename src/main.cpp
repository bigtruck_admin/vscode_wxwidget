#include <wx/wx.h>
#include <wx/display.h>

#include <MainFrame.h>

// #ifdef __APPLE__
// #include <Security/Authorization.h>
// #include <Security/AuthorizationTags.h>
// #endif

class MyApp : public wxApp
{
public:
    bool OnInit() override;


};

wxIMPLEMENT_APP(MyApp);


bool MyApp::OnInit()
{
    wxDisplay display;
    wxRect screenRect = display.GetClientArea(); // 获取主屏幕的客户区域大小
    int screenWidth = screenRect.GetWidth() * 0.8;
    int screenHeight = screenRect.GetHeight() * 0.8;
// #ifdef __APPLE__

//     AuthorizationRef authorizationRef;
//     AuthorizationItem right = {kAuthorizationRightExecute, 0, NULL, 0};
//     AuthorizationRights rights = {1, &right};
//     AuthorizationFlags flags = kAuthorizationFlagInteractionAllowed | kAuthorizationFlagExtendRights;
//     AuthorizationEnvironment environment = {0, NULL};

//     // OSStatus status = AuthorizationCreate(NULL, kAuthorizationEmptyEnvironment, kAuthorizationFlagDefaults, &authorizationRef);
//     OSStatus status = AuthorizationCreate(&rights, kAuthorizationEmptyEnvironment, flags, &authorizationRef);
//     if (status != errAuthorizationSuccess) {
//         // 处理授权创建失败的情况
//         wxLogError("获取授权失败.");
//         return 1;
//     }

//     status = AuthorizationCopyRights(authorizationRef, &rights, kAuthorizationEmptyEnvironment, flags, NULL);
//     if (status != errAuthorizationSuccess) {
//         // 处理授权复制失败的情况
//         AuthorizationFree(authorizationRef, kAuthorizationFlagDefaults);
//         wxLogError("授权复制失败.");
//         return 1;
//     }
// #endif
    MainFrame *frame = new MainFrame();
    // frame->SetTitle(wxT("Tool"));
    frame->SetSize(screenWidth,screenHeight);
    frame->Center();
    frame->Show(true);
// #ifdef __APPLE__
//     AuthorizationFree(authorizationRef, kAuthorizationFlagDefaults);
// #endif
    return true;
}

