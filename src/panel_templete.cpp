#include "panel_templete.h"

PanelTemplete::PanelTemplete(const wxString &title, wxWindowID id, wxWindow *parent)
    : wxPanel(parent,id,wxDefaultPosition,wxDefaultSize,wxTAB_TRAVERSAL)
{
    SetSize(200, 300);

    layout = new wxBoxSizer(wxVERTICAL);
    layout->Add(new wxButton(this, wxID_ANY, "Test"), 1, wxEXPAND);
    SetSizer(layout);


    paneInfo.Caption(title);
    paneInfo.Dock();
    paneInfo.Dockable(true);
    paneInfo.CloseButton(true);
    paneInfo.MaximizeButton(true);
    paneInfo.MinimizeButton(true);
    paneInfo.DestroyOnClose(false);
    // paneInfo.Direction(wxLeft);

}


wxAuiPaneInfo &PanelTemplete::GetPaneInfo()
{
    return paneInfo;
}


void PanelTemplete::SetShow(bool show)
{
    Show(show);
}
