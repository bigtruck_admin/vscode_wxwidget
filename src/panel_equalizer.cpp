#include "panel_equalizer.h"


wxBEGIN_EVENT_TABLE(PanelEqualizer, wxPanel)
    MY_EVT_EQUALIZER(wxID_ANY, PanelEqualizer::OnEqualizerEvent)
    MY_EVT_EQUALIZER_FREQUENCY(wxID_ANY, PanelEqualizer::OnEqualizerFrequencyEvent)
    MY_EVT_EQUALIZER_TYPE(wxID_ANY, PanelEqualizer::OnEqualizerTypeEvent)
    MY_EVT_EQUALIZER_GAIN(wxID_ANY, PanelEqualizer::OnEqualizerGainEvent)
    MY_EVT_EQUALIZER_QFACTOR(wxID_ANY, PanelEqualizer::OnEqualizerQFactorEvent)
wxEND_EVENT_TABLE()

PanelEqualizer::PanelEqualizer(const wxString &title,int sample_rate, wxWindowID id, wxWindow *parent)
    : wxPanel(parent,id,wxDefaultPosition,wxDefaultSize,wxTAB_TRAVERSAL)
{
    SetSize(200, 300);

    equalizer = new GraphicEqualizer(this, wxID_ANY, "Graphic EQ",wxDefaultPosition,wxSize(100,100));
    layout = new wxBoxSizer(wxVERTICAL);
    layout->Add(equalizer, 1, wxEXPAND);
    SetSizer(layout);

    // Bind(myEVT_EQUALIZER, &PanelEqualizer::OnEqualizerEvent, this);

    paneInfo.Caption(title);
    paneInfo.Dock();
    paneInfo.Dockable(true);
    paneInfo.CloseButton(true);
    paneInfo.MaximizeButton(true);
    paneInfo.MinimizeButton(true);
    paneInfo.DestroyOnClose(false);
    // paneInfo.Direction(wxLeft);
    equalizer->SetSampleRate(sample_rate);
}


wxAuiPaneInfo &PanelEqualizer::GetPaneInfo()
{
    return paneInfo;
}


void PanelEqualizer::SetShow(bool show)
{
    Show(show);
}

void PanelEqualizer::OnEqualizerEvent(MyEventEqualizer &event)
{
    if(event.GetIndex() >= 0)
    {
    }
    event.Skip();
}

void PanelEqualizer::OnEqualizerFrequencyEvent(MyEventEqualizer &event)
{
    if(event.GetIndex() >= 0)
    {
    }
    event.Skip();
}
void PanelEqualizer::OnEqualizerGainEvent(MyEventEqualizer &event)
{
    if(event.GetIndex() >= 0)
    {
    }
    event.Skip();
}
void PanelEqualizer::OnEqualizerQFactorEvent(MyEventEqualizer &event)
{
    if(event.GetIndex() >= 0)
    {
    }
    event.Skip();
}
void PanelEqualizer::OnEqualizerTypeEvent(MyEventEqualizer &event)
{
    if(event.GetIndex() >= 0)
    {
    }
    event.Skip();
}

void PanelEqualizer::AddFilter(FilterType type, double frequency, double gain,double qfactor)
{
    equalizer->AddFilter(type,frequency,gain,qfactor);
}