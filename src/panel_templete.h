#ifndef __PANEL_TEMPLETE_H__
#define __PANEL_TEMPLETE_H__

#include <wx/wx.h>
#include <wx/aui/aui.h>
#include <wx/wrapsizer.h>

class PanelTemplete : public wxPanel
{
public:
    PanelTemplete(const wxString &title, wxWindowID id = wxID_ANY, wxWindow *parent = nullptr);

private:
    wxAuiPaneInfo paneInfo;
    wxBoxSizer *layout;
public:
    wxAuiPaneInfo &GetPaneInfo();
    void SetShow(bool show);

private:

};


#endif
