#include "panel_logger.h"
#include <cstdio>
#if defined _WIN32
#include <windows.h>
#endif

PanelLogger::PanelLogger(const wxString &title, wxWindowID id, wxWindow *parent)
    : wxPanel(parent,id,wxDefaultPosition,wxDefaultSize,wxTAB_TRAVERSAL)
{
    SetSize(600, 200);
    // text_log = new wxTextCtrl(this, wxID_ANY,"",wxDefaultPosition,wxDefaultSize,wxTE_MULTILINE | wxTE_RICH2);
    text_log = new wxListBox(this, wxID_ANY,wxDefaultPosition,wxDefaultSize,0,0,wxLB_HSCROLL);
    //std::cout的输出重新定向到text_log
    // std::ios::sync_with_stdio(true);
    // freopen(NULL, "w", stdout);
    textOutputStream = new TextCtrlOutputStream(text_log);
    std::cout.rdbuf(textOutputStream);
    std::cerr.rdbuf(textOutputStream);
    std::clog.rdbuf(textOutputStream);

    textOutputLog = new LboxLogger(text_log, wxLog::GetActiveTarget());
    wxLog::SetActiveTarget(textOutputLog);

    btn_clear = new wxButton(this, wxID_ANY, wxT("Clear"));
    btn_clear->Bind(wxEVT_BUTTON,&PanelLogger::OnButtonClear,this);

    layout = new wxBoxSizer(wxVERTICAL);
    wrap_layout = new wxWrapSizer(wxHORIZONTAL);
    wrap_layout->Add(btn_clear, 1, wxEXPAND);
    layout->Add(wrap_layout, 0, wxEXPAND);
    layout->Add(text_log, 1, wxEXPAND);
    SetSizer(layout);

    // paneInfo.Name("FloatingPanel Demo");
    paneInfo.Caption(title);
    paneInfo.Dock();
    paneInfo.Dockable(true);
    paneInfo.CloseButton(true);
    paneInfo.MaximizeButton(true);
    paneInfo.MinimizeButton(true);
    paneInfo.DestroyOnClose(false);
    // paneInfo.Direction(wxBottom);

    // logTextStyle.SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
    // logTextStyle.SetTextColour(*wxBLACK);
    // text_log->SetDefaultStyle(logTextStyle);
    // text_log->SetEditable(false);
    // text_log->SetBackgroundColour(wxColour(169,169,169));
    // AppendLog(Log,"log");
    // AppendLog(Warning,"warning");
    // AppendLog(Error,"error");

    // wxLogMessage("Message\n");
    // wxLogWarning("Warning\n");
    // wxLogInfo("Info\n");
    // wxLogInfo(wxString::Format("Logger %s\n",title));
}

PanelLogger::~PanelLogger()
{
    delete textOutputLog;
}


wxAuiPaneInfo &PanelLogger::GetPaneInfo()
{
    return paneInfo;
}


void PanelLogger::SetShow(bool show)
{
    Show(show);
}

void PanelLogger::OnButtonClear(wxCommandEvent& event)
{
    int ret = wxMessageBox(
        wxT("Clear?"),
        wxT("Clear Log"),
        wxYES_NO | wxICON_WARNING,
        this
    );
    if(ret == wxYES)
    {
        text_log->Clear();
    }
}

void PanelLogger::AppendLog(LoggerType type,const wxString &str)
{
    // wxTextAttr textAttr;
    // switch(type)
    // {
    //     case Log:
    //         textAttr.SetTextColour(*wxBLACK);
    //         textAttr.SetBackgroundColour(wxColour(169,169,169));
    //         break;
    //     case Warning:
    //         textAttr.SetTextColour(wxColour(255,255,0));
    //         textAttr.SetBackgroundColour(wxColour(112, 128, 144));
    //         break;
    //     case Error:
    //         textAttr.SetTextColour(*wxRED);
    //         textAttr.SetBackgroundColour(wxColour(0,0,0));
    //         break;
    //     default:
    //         textAttr.SetTextColour(*wxBLACK);
    //         textAttr.SetBackgroundColour(wxColour(169,169,169));
    //     break;
    // }
    // text_log->SetDefaultStyle(textAttr);
    // text_log->WriteText(str);
    // // text_log->SetDefaultStyle(logTextStyle);
    // text_log->WriteText("\n");
    // // text_log->ShowPosition(text_log->GetLastPosition());
    // text_log->SetInsertionPointEnd();
    text_log->Append(str);
}

wxListBox* PanelLogger::GetTextCtrl()
{
    return text_log;
}