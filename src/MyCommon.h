#ifndef __MYCOMMON_H__
#define __MYCOMMON_H__

#include <wx/wx.h>
#include <string>
#include <iostream>

#define USER_ID_START   wxID_HIGHEST

#define ID_START_MAINFREAM          (USER_ID_START + 0)
#define ID_START_PANEL_DEVICE       (USER_ID_START + 100)
#define ID_START_PANEL_REVERB       (USER_ID_START + 200)
#define ID_START_PANEL_CHORUS       (USER_ID_START + 300)
#define ID_START_PANEL_DELAY        (USER_ID_START + 400)
#define ID_START_PANEL_COMPRESS     (USER_ID_START + 500)
#define ID_START_PANEL_EQUALIZER    (USER_ID_START + 600)
#define ID_START_PANEL_DISTORTION   (USER_ID_START + 700)
#define ID_START_PANEL_WAH          (USER_ID_START + 800)
#define ID_START_PANEL_PRESET       (USER_ID_START + 900)


class MyCommon
{
private:
    /* data */
public:
    MyCommon(/* args */){

    }
    ~MyCommon(){

    }
};

class ProgressDialog : public wxDialog
{
public:
    ProgressDialog(wxWindow *parent = nullptr, wxWindowID id=wxID_ANY, const wxString& title=wxT(""), const wxString& message=wxT(""), int maximum = 100)
        : wxDialog(parent, id, title), m_gauge(new wxGauge(this, wxID_ANY, maximum))
    {
        wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
        m_staticText = new wxStaticText(this, wxID_ANY, "0%");
        sizer->Add(new wxStaticText(this, wxID_ANY, message), 0, wxALL, 5);
        sizer->Add(m_gauge, 0, wxALL | wxEXPAND, 5);
        sizer->Add(m_staticText, 0, wxALL | wxEXPAND, 5);
        SetSizerAndFit(sizer);
        CenterOnScreen();
    }

    void Update(int value)
    {
        m_gauge->SetValue(value);
        m_staticText->SetLabel(wxString::Format("%d%%", value));
        Refresh();
    }

private:
    wxGauge* m_gauge;
    wxStaticText* m_staticText;
};


static inline std::string mDoubleToString(double value){
    std::string str;
    str = std::to_string(value);
    size_t dotIndex = str.find('.');
    if(dotIndex != std::string::npos)
    {
        size_t nonZeroIndex = str.find_last_not_of('0', (unsigned long long)str.length() - 1);
        if (nonZeroIndex != std::string::npos) {
            str = str.substr(0, nonZeroIndex+1); // 截取字符串，去掉末尾的'0'
        }
        if(str.at(str.length()-1) == '.')
        {
            str = str.substr(0, str.length()-1);
        }
    }
    return str;
}


#endif
