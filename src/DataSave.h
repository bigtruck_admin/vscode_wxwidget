#ifndef __DATASAVE_H__
#define __DATASAVE_H__

#include <wx/wx.h>
#include <wx/xml/xml.h>

class DataSave : public wxObject
{
public:
    typedef enum{
        NODE_REVERB = 1,
        NODE_CHORUS,
        NODE_DELAY,
        NODE_WAH,
        NODE_DISTORTION,
        NODE_COMPRESS,
        NODE_EQUALIZER
    }node_type;

public:
    DataSave(wxString &path);
    ~DataSave();
    // 保存XML文件
    bool Save(void);

    static wxXmlNode *FindChildNode(wxXmlNode *node, const char *name,bool autoCreate = true);
    static bool NodeAttributesSetValue(wxXmlNode *node, const char *attr_name, const char *value,bool autoCreate = true);
    static bool NodeAttributesGetValue(wxXmlNode *node, const char *attr_name, wxString &value);
    wxXmlNode *GetEffectParamNode(node_type type);
private:
    wxString file_path;
    wxXmlDocument xmlDoc;
    wxXmlNode *save_node_reverb;
    wxXmlNode *save_node_chorus;
    wxXmlNode *save_node_delay;
    wxXmlNode *save_node_wah;
    wxXmlNode *save_node_distortion;
    wxXmlNode *save_node_distortion2;

};


#endif
