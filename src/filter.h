#ifndef __FILTER_H__
#define __FILTER_H__

static const char *filter_type_name[]=
{
    "Error",
    "Lowpass",
    "Hightpass",
    "Bandpass",
    "Peak",
    "Notch",
    "Lowshelf",
    "Highshelf",
    "Error",
    "Error",
    "Error",
    "Error",
};

typedef enum{
    Lowpass=1,
    Hightpass,
    Bandpass,
    Peak,
    Notch,
    Lowshelf,
    Highshelf,
}FilterType;

typedef struct{
    double b0;
    double b1;
    double b2;
    double a1;
    double a2;
}FilterCoefficient;

typedef struct
{
    FilterType type;
    double frequency;
    double gain;
    double qfactor;
}FilterParam;

#ifdef __cplusplus
extern "C" {
#endif

void CalculationCoefficient(FilterType type,double frequency,double gain,double qfactor,double sampleRate,FilterCoefficient *coefficient);
int GetFrequencyResponseData(double start_freq, double end_freq,double freq_step,double sampleRate,FilterCoefficient *coefficient,double *out_buff);


#ifdef __cplusplus
}
#endif

#endif
