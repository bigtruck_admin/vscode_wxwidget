#include "panel_preset.h"

wxDEFINE_EVENT(myEVT_PRESET_INDEX, MyEventPreset);
wxDEFINE_EVENT(myEVT_PRESET_EFFECT_STATUS_UPDATE, MyEventPreset);

PanelPreset::PanelPreset(const wxString &title, wxWindowID id, wxWindow *parent)
    : wxPanel(parent,id,wxDefaultPosition,wxDefaultSize,wxTAB_TRAVERSAL)
{
    SetSize(200, 300);

    // layout = new wxBoxSizer(wxVERTICAL);
    // layout->Add(new wxButton(this, wxID_ANY, "Test"), 1, wxEXPAND);
    // SetSizer(layout);

    wxFlexGridSizer *layout_main = new wxFlexGridSizer(preset_max,7,wxSize(3,3));

    wxButton *btn;
    wxCheckBox *chk;
    int cid = id+1;
    for(int i=0; i<preset_max; i++)
    {
        ID_BTN_PRESET[i] = cid++;
        ID_CHK_CHR[i] = cid++;
        ID_CHK_REV[i] = cid++;
        ID_CHK_DLY[i] = cid++;
        ID_CHK_DST1[i] = cid++;
        ID_CHK_DST2[i] = cid++;
        ID_CHK_WAH[i] = cid++;
        btn = new wxButton(this, ID_BTN_PRESET[i], wxString::Format("Preset %d",i+1));
        btn->Bind(wxEVT_BUTTON, &PanelPreset::OnBtnPreset, this);
        layout_main->Add(btn  ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
        chk = new wxCheckBox(this  , ID_CHK_DST1[i]  , "Distortion 1");
        chk->Bind(wxEVT_CHECKBOX, &PanelPreset::OnChkChange, this);
        layout_main->Add(chk  ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
        chk = new wxCheckBox(this  , ID_CHK_DST2[i]  , "Distortion 2");
        chk->Bind(wxEVT_CHECKBOX, &PanelPreset::OnChkChange, this);
        layout_main->Add(chk  ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
        chk = new wxCheckBox(this  , ID_CHK_REV[i]  , "Reverb");
        chk->Bind(wxEVT_CHECKBOX, &PanelPreset::OnChkChange, this);
        layout_main->Add(chk  ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
        chk = new wxCheckBox(this  , ID_CHK_CHR[i]  , "Chorus");
        chk->Bind(wxEVT_CHECKBOX, &PanelPreset::OnChkChange, this);
        layout_main->Add(chk  ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
        chk = new wxCheckBox(this  , ID_CHK_DLY[i]  , "Delay");
        chk->Bind(wxEVT_CHECKBOX, &PanelPreset::OnChkChange, this);
        layout_main->Add(chk  ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
        chk = new wxCheckBox(this  , ID_CHK_WAH[i]  , "Wah");
        chk->Bind(wxEVT_CHECKBOX, &PanelPreset::OnChkChange, this);
        layout_main->Add(chk  ,0,wxALIGN_CENTER_VERTICAL | wxEXPAND);
        effect_status_reverb[i] = 0;
        effect_status_chorus[i] = 0;
        effect_status_delay[i] = 0;
        effect_status_wah[i] = 0;
        effect_status_distortion1[i] = 0;
        effect_status_distortion2[i] = 0;
    }
    SetSizer(layout_main);

    paneInfo.Caption(title);
    paneInfo.Dock();
    paneInfo.Dockable(true);
    paneInfo.CloseButton(true);
    paneInfo.MaximizeButton(true);
    paneInfo.MinimizeButton(true);
    paneInfo.DestroyOnClose(false);
    // paneInfo.Direction(wxLeft);

    param_event.SetEventObject(this);
    SetPreset(0);
}


wxAuiPaneInfo &PanelPreset::GetPaneInfo()
{
    return paneInfo;
}


void PanelPreset::SetShow(bool show)
{
    Show(show);
}

void PanelPreset::SetPreset(int preset)
{
    wxCheckBox *chk;
    wxButton *btn;
    bool en;
    // wxButton *cbtn = wxDynamicCast(FindWindowById(ID_BTN_PRESET[preset]),wxButton);

    for(int i=0; i<preset_max; i++)
    {
        en = false;
        btn = wxDynamicCast(FindWindowById(ID_BTN_PRESET[i]),wxButton);
        if(i == preset)
        {
            btn->SetBackgroundColour(wxColour(0,150,100));
            en = true;
            param_event.SetPresetIndex(i);
            param_event.SetEffectStatusDistortion1(effect_status_distortion1[i]);
            param_event.SetEffectStatusDistortion2(effect_status_distortion2[i]);
            param_event.SetEffectStatusReverb(effect_status_reverb[i]);
            param_event.SetEffectStatusChorus(effect_status_chorus[i]);
            param_event.SetEffectStatusDelay(effect_status_delay[i]);
            param_event.SetEffectStatusWah(effect_status_wah[i]);
            param_event.SetEventType(myEVT_PRESET_INDEX);
            ProcessWindowEvent(param_event);
        }
        else
        {
            btn->SetBackgroundColour(wxColour(255,255,255));
            en = false;
        }
        chk = wxDynamicCast(FindWindowById(ID_CHK_DST1[i]),wxCheckBox);
        chk->Enable(en);
        chk = wxDynamicCast(FindWindowById(ID_CHK_DST2[i]),wxCheckBox);
        chk->Enable(en);
        chk = wxDynamicCast(FindWindowById(ID_CHK_REV[i]),wxCheckBox);
        chk->Enable(en);
        chk = wxDynamicCast(FindWindowById(ID_CHK_CHR[i]),wxCheckBox);
        chk->Enable(en);
        chk = wxDynamicCast(FindWindowById(ID_CHK_DLY[i]),wxCheckBox);
        chk->Enable(en);
        chk = wxDynamicCast(FindWindowById(ID_CHK_WAH[i]),wxCheckBox);
        chk->Enable(en);
    }
}

void PanelPreset::OnBtnPreset(wxCommandEvent& event)
{
    // wxButton *btn;
    // wxCheckBox *chk;
    // bool en;
    wxWindowID id = event.GetId();
    // wxButton *cbtn = wxDynamicCast(FindWindowById(id),wxButton);
    for(int i=0; i<preset_max; i++)
    {
        if(id == ID_BTN_PRESET[i])
        {
            SetPreset(i);
            break;
        }
    }
}

int PanelPreset::GetPreset(void)
{
    return current_preset;
}

void PanelPreset::OnChkChange(wxCommandEvent& event)
{
    wxWindowID id = event.GetId();
    wxCheckBox *chk = wxDynamicCast(FindWindowById(id),wxCheckBox);
    for(int i=0; i<preset_max; i++)
    {
        if(id == ID_CHK_DST1[i])
        {
            effect_status_distortion1[i] = chk->IsChecked();
            param_event.SetEffectStatusDistortion1(effect_status_distortion1[i]);
        }
        else if(id == ID_CHK_DST2[i])
        {
            effect_status_distortion2[i] = chk->IsChecked();
            param_event.SetEffectStatusDistortion2(effect_status_distortion2[i]);
        }
        else if(id == ID_CHK_REV[i])
        {
            effect_status_reverb[i] = chk->IsChecked();
            param_event.SetEffectStatusReverb(effect_status_reverb[i]);
        }
        else if(id == ID_CHK_CHR[i])
        {
            effect_status_chorus[i] = chk->IsChecked();
            param_event.SetEffectStatusChorus(effect_status_chorus[i]);
        }
        else if(id == ID_CHK_DLY[i])
        {
            effect_status_delay[i] = chk->IsChecked();
            param_event.SetEffectStatusDelay(effect_status_delay[i]);
        }
        else if(id == ID_CHK_WAH[i])
        {
            effect_status_wah[i] = chk->IsChecked();
            param_event.SetEffectStatusWah(effect_status_wah[i]);
        }
        param_event.SetEventType(myEVT_PRESET_EFFECT_STATUS_UPDATE);
        ProcessWindowEvent(param_event);
    }
}

void PanelPreset::OnBtnMouseEnter(wxMouseEvent& event)
{
    wxButton* button = dynamic_cast<wxButton*>(event.GetEventObject());
    if (button)
    {
    }
}

void PanelPreset::PresetEffectStatusInit(int preset,int effect,int status)
{
    wxCheckBox *chk;
    if(preset < preset_max)
    {
        if(effect == EFFECT_INDEX_REVERB)
        {
            effect_status_reverb[preset] = status;
            chk = wxDynamicCast(FindWindowById(ID_CHK_REV[preset]),wxCheckBox);
            chk->SetValue(status);
        }
        else if(effect == EFFECT_INDEX_CHORUS)
        {
            effect_status_chorus[preset] = status;
            chk = wxDynamicCast(FindWindowById(ID_CHK_CHR[preset]),wxCheckBox);
            chk->SetValue(status);
        }
        else if(effect == EFFECT_INDEX_DELAY)
        {
            effect_status_delay[preset] = status;
            chk = wxDynamicCast(FindWindowById(ID_CHK_DLY[preset]),wxCheckBox);
            chk->SetValue(status);
        }
        else if(effect == EFFECT_INDEX_DISTORTION1)
        {
            effect_status_distortion1[preset] = status;
            chk = wxDynamicCast(FindWindowById(ID_CHK_DST1[preset]),wxCheckBox);
            chk->SetValue(status);
        }
        else if(effect == EFFECT_INDEX_DISTORTION2)
        {
            effect_status_distortion2[preset] = status;
            chk = wxDynamicCast(FindWindowById(ID_CHK_DST2[preset]),wxCheckBox);
            chk->SetValue(status);
        }
        else if(effect == EFFECT_INDEX_WAH)
        {
            effect_status_wah[preset] = status;
            chk = wxDynamicCast(FindWindowById(ID_CHK_WAH[preset]),wxCheckBox);
            chk->SetValue(status);
        }
    }
}