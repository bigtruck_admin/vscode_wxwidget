#ifndef __PANEL_DEVICE_H__
#define __PANEL_DEVICE_H__

#include <wx/wx.h>
#include <wx/aui/aui.h>
#include <wx/wrapsizer.h>
#include <wx/listctrl.h>
#include <wx/textctrl.h>
#include <wx/timer.h>
#include "USBCommunication.h"
#include "MyCommon.h"
#include "CommandProcess.h"

class PanelDevice : public wxPanel
{

public:
    typedef enum
    {
        SUCCESS = 0,
        TIMEOUT,
        FAIL
    }Status;

public:
    PanelDevice(const wxString &title, wxWindowID id = wxID_ANY, wxWindow *parent = nullptr);
    ~PanelDevice();
    void OnDestroy(wxWindowDestroyEvent& event);

private:
    wxAuiPaneInfo panel_info;
    wxBoxSizer *layout_main;
    wxBoxSizer *layout_usb;
    wxListCtrl *list_device;
    wxButton *btn_find_bt;
    wxTextCtrl *text_usb_vid, *text_usb_pid, *text_usb_interface_num, *text_usb_in_ep, *text_usb_out_ep;
    wxStaticText *text_usb_vid_label, *text_usb_pid_label, *text_usb_interface_num_label, *text_usb_in_ep_label, *text_usb_out_ep_label;
    wxStaticBoxSizer *group_bt;
    wxStaticBoxSizer *group_usb;
    wxButton *btn_usb_link;
    wxTimer *tim_usb_delay;
    USBCommunication *usb_comm;
    CommandProcess *cmd_proc;

    unsigned int usb_device_pid;
    unsigned int usb_device_vid;
    unsigned char usb_device_in_ep;
    unsigned char usb_device_out_ep;
    char usb_device_interface_num;

public:
    wxAuiPaneInfo &GetPaneInfo();
    void SetShow(bool show);
    void OnUSBDeviceArrival(MyEventMainFrame& event);
    void OnUSBDeviceRemove(MyEventMainFrame& event);
    void USBDeviceArrival(unsigned int vid, unsigned int pid,wxString serialNum);
    void USBDeviceRemove(unsigned int vid, unsigned int pid,wxString serialNum);

    int WriteData(const char *data,int len);
    int ReadData(char *data,int len);

private:
    void OnFindBT(wxCommandEvent& event);
    void OnConnectUSB(wxCommandEvent& event);
    void OnTimerUSBDelay(wxTimerEvent& event);
    void ScanBluetoothDevices(void);
public:

    wxDECLARE_EVENT_TABLE();
};


#endif
